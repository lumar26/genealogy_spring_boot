package rs.ac.bg.fon.np.genealogy_spring_boot.events.listener;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import rs.ac.bg.fon.np.genealogy_spring_boot.events.event.RegistrationCompleteEvent;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.UserService;

import java.util.UUID;

@AllArgsConstructor
@Component
public class RegistrationEventListener  implements ApplicationListener<RegistrationCompleteEvent> {

    // TODO: this should be loaded from config file
    private static final String CONFIRMATION_PATH = "/auth/registration/verify?token=";
    private static final String EMAIL_MESSAGE = "Click a link to verify your account on genealogy app:";

    private final UserService userService;
    private JavaMailSender mailSender;
    @Override
    public void onApplicationEvent(RegistrationCompleteEvent event) {
        confirmRegistration(event);
    }

    private void confirmRegistration(RegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        userService.createVerificationToken(user, token);

        String subject = "Registration Confirmation";
        String confirmationUrl = CONFIRMATION_PATH + token;

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject(subject);
        email.setText(EMAIL_MESSAGE + "\r\n" + "http://localhost:8080" + confirmationUrl);
        mailSender.send(email);
    }
}

