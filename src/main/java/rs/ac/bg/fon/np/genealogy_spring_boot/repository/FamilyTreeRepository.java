package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

import java.util.List;

/**
 * Interface that extends JpaRepository which provides all methods for basic CRUD operations, specialized for working with FamilyTree entity.
 *
 * @author lumar26
 * @version 1.0
 */
@Repository
public interface FamilyTreeRepository extends JpaRepository<FamilyTree, Long> {
    /**
     * Method for retrieving all family trees whose owner is provided user.
     *
     * @param owner User that is creator and owner of family tree
     * @return Collection of family trees
     */
    List<FamilyTree> findAllByUser(User owner);
}
