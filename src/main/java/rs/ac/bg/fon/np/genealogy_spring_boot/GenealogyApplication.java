package rs.ac.bg.fon.np.genealogy_spring_boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenealogyApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenealogyApplication.class, args);
	}

}
