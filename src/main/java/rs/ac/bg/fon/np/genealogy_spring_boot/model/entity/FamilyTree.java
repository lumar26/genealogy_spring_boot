package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Entity that represents basic information about family tree in system.
 * Each family tree must be assigned to exactly one user.
 * Each member in family tree must be assigned to a family tree.
 *
 * @author lumar26
 * @version 1.0
 */
@Entity
@Table(name = "family_tree")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Accessors(chain = true)
public class FamilyTree {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private Long id;

    @EqualsAndHashCode.Include
    @NotNull
    @NotBlank(message = "Title for family tree is mandatory")
    private String title;

    @EqualsAndHashCode.Include
    private String description;

    @JsonBackReference(value = "user-familyTree")
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @NotNull(message = "Family tree has to have defined user who created it")
    private User user;

    @JsonManagedReference(value = "member-familyTree")
    @ToString.Exclude
    @OneToMany(mappedBy = "familyTree", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Member> members;

    public FamilyTree(String title, String description, User user) {
        this.title = title;
        this.description = description;
        this.user = user;
    }

    public void addMember(Member member) {
        members.add(member);
        member.setFamilyTree(this);
    }

    public void removeAllMembers() {
        if (members != null)
            members.forEach(member -> member.setFamilyTree(null));
        members = null;
    }

    public void removeMember(Member member) {
        members.remove(member);
        member.setFamilyTree(null);
    }
}
