package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Accessors(chain = true)
public class FamilyTreeInfoResponse {
    private Long id;
    private String title;
    private String description;
    private String owner;

    public static FamilyTreeInfoResponse of(FamilyTree familyTree){
        return new FamilyTreeInfoResponse(familyTree.getId(), familyTree.getTitle(), familyTree.getDescription(), familyTree.getUser().getUsername());
    }

}
