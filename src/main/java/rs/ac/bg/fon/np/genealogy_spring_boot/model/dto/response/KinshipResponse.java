package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Accessors(chain = true)
public class KinshipResponse {

    public enum Ancestor {
        FIRST("Отац/Мајка"),
        SECOND("Деда/Баба"),
        THIRD("Прадеда/Прабаба"),
        FOURTH("Чукундеда/Чукунбаба"),
        FIFTH("Наврдеда/Наврбаба"),
        SIXTH("Курђел/Курђела"),
        SEVENTH("Аскурђел/Аскурђела"),
        EIGHTH("Курђуп/Курђупа"),
        NINTH("Курлебало/Курлебала"),
        TENTH("Сурдуков/Сурдукова"),
        ELEVENTH("Сурдепач/Сурдепача"),
        TWELFTH("Парђупан/Парђупана"),
        THIRTEENTH("Ожмикур/Ожмикура"),
        FOURTEENTH("Курајбер/Курајбера"),
        FIFTEENTH("Сајкатав/Сајкатава"),
        SIXTEENTH("Бели орао/Бела пчела"),
        ;
        private final String value;

        Ancestor(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public String getFemale(){
            return value.split("/")[1];
        }

        public String getMale(){
            return value.split("/")[0];
        }

    }

    public enum Type {
        LATERAL, VERTICAL
    }

    private String member1;
    private String member2;
    private Type type;
    private int degree;
    private String member1Role;
    private String member2Role;
    private boolean sameLevelCousins;
}
