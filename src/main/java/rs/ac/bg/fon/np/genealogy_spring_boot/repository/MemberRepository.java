package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;

import java.util.List;

/**
 * Interface that extends JpaRepository which provides all methods for basic CRUD operations, specialized for working with Member entity.
 *
 * @author lumar26
 * @version 1.0
 */
@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

    /**
     * Method for deleting member regardless of existing references to that Member object
     *
     * @param id Unique identifier by which member will be deleted.
     */
    @Override
    @Modifying
    @Query("delete from Member m where m.id = :id")
    void deleteById(Long id);

    /**
     * Method for retrieving all members of family tree from database.
     *
     * @param familyTreeId Identifier of family tree to which member belongs
     * @return All members in family tree with given id.
     */
    List<Member> findAllByFamilyTree_Id(Long familyTreeId);
    List<Member> findAllByFamilyTree(FamilyTree familyTree);

    @Query("select m from Member m where m.ancestry like :lastAncestorId")
    List<Member> findAllDescendants(String lastAncestorId);

    List<Member> findAllByAncestryEndingWith(String lastAncestorId);

    List<Member> findAllByIdNotIn(List<Long> ids);


}
