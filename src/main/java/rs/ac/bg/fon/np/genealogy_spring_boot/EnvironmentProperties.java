package rs.ac.bg.fon.np.genealogy_spring_boot;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app")
@Data
public class EnvironmentProperties {
    private int jwt_expiration_in_millis = 15 * 60 * 1000;
    private int refresh_token_expiration_in_months = 6;
    private int verification_token_expiration_in_days = 10;
}
