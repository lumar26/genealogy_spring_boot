package rs.ac.bg.fon.np.genealogy_spring_boot.family_tree.dto;

import lombok.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DTO representing response that contains basic info about family tree.
 * This DTO is used as response after CRUD operations on family tree.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FamilyTreeResponseDto {
    @NotNull
    private Long id;
    @NotBlank
    private String title;
    private String description;

    public static FamilyTreeResponseDto of(FamilyTree entity){
        return new FamilyTreeResponseDto(entity.getId(),
                entity.getTitle(),
                entity.getDescription());
    }
}
