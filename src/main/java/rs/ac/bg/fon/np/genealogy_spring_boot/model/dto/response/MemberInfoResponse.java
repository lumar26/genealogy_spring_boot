package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.validation.ValidMemberDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * DTO representing response that contains detailed info about member, i.e. all the info contained in MemberInfo class.
 * This DTO is used when all info about the person that is this member is required.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidMemberDto
public class MemberInfoResponse {
    @NotBlank(message = "Member's name must be defined")
    private String name;
    @NotBlank(message = "Member's surname must be defined")
    private String surname;
    @NotBlank(message = "Member's gender must be defined")
    private String gender;
    @NotNull(message = "Member's date of birth must be defined")
    private LocalDate birthDate;
    private LocalDate deathDate;
    private String birthPlace;
    private String deathPlace;
    private String country;
    private String postalCode;
    private String cityOrMunicipality;
    private String street;
    private String number;

    public static MemberInfoResponse of(MemberInfo info) {
        return new MemberInfoResponse(
                info.getName()
                , info.getSurname()
                , info.getGender().name()
                , info.getBirthDate()
                , info.getDeathDate()
                , info.getBirthPlace()
                , info.getDeathPlace()
                , info.getCurrentAddress().getCountry()
                , info.getCurrentAddress().getPostalCode()
                , info.getCurrentAddress().getCityOrMunicipality()
                , info.getCurrentAddress().getStreet()
                , info.getCurrentAddress().getNumber());
    }

    public MemberInfoResponse(MemberInfo info) {
        name = info.getName();
        surname = info.getSurname();
        gender = info.getGender().name();
        birthDate = info.getBirthDate();
        deathDate = info.getDeathDate();
        birthPlace = info.getBirthPlace();
        deathPlace = info.getDeathPlace();
        country = info.getCurrentAddress().getCountry();
        postalCode = info.getCurrentAddress().getPostalCode();
        cityOrMunicipality = info.getCurrentAddress().getCityOrMunicipality();
        street = info.getCurrentAddress().getStreet();
        number = info.getCurrentAddress().getNumber();
    }
}
