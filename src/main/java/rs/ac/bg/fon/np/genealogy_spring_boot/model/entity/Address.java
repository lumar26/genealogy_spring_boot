package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Embedded object that is part of MemberInfo object, contains all information about member's current address.
 *
 * @author lumar26
 * @version 1.0
 */
@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Address {
    /**
     * Country in which member currently lives.
     */
    private String country;

    /**
     * Postal code of municipality in which member currently lives.
     */
    @EqualsAndHashCode.Include
    private String postalCode;

    /**
     * City or municipality in which member currently lives.
     */
    @Column(name = "city_municipality")
    private String cityOrMunicipality;

    /**
     * Street in which member currently lives.
     */
    @EqualsAndHashCode.Include
    private String street;

    /**
     * Street number in which member currently lives.
     */
    @EqualsAndHashCode.Include
    private String number;
}
