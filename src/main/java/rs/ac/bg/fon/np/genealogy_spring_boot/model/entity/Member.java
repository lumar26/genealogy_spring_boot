package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity class that contains information about member of family tree as well as belonging to family tree and owner, and also relationships with other members of family tree
 *
 * @author lumar26
 * @version 1.0
 */
@Entity
@Table(name = "member")
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@Accessors(chain = true)
public class Member {
    /**
     * Unique numeric identifier of member in database
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private Long id;

    /**
     * Ancestry represents chain of ancestors startig with father, grandfather and so on. This list is representet as string where ids of ancestor are separated with '/'.
     * If member is root of family tree then his ancestry is equal to '/'.
     * Only male ancestor's ids are contained in ancestry field.
     */
    @EqualsAndHashCode.Include
    private String ancestry = "/";

    /**
     * User who created this member.
     */
    @JsonBackReference(value = "user-member")
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Family tree to which this member belongs.
     */
    @JsonBackReference(value = "member-familyTree")
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "family_tree_id")
    private FamilyTree familyTree;

    /**
     * Object that contains information about member such as name, surname, gender etc.
     */
    @Embedded
    @EqualsAndHashCode.Include
    private MemberInfo info;

    /**
     * Parent of this member which can be either male or female. This field is optional since member can be alone in family tree or can be defined as root.
     */
    @JsonBackReference(value = "member-parent")
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Member parent;

    /**
     * List of children of member, if this member has any.
     */
    @JsonManagedReference(value = "member-parent")
    @ToString.Exclude
    @OneToMany(mappedBy = "parent",
//            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH},
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private List<Member> children;


    public Member(User user, FamilyTree familyTree, MemberInfo info, Member parent, List<Member> children) {
        this.user = user;
        this.familyTree = familyTree;
        this.info = info;
        this.parent = parent;
        this.children = children;
    }

    public Member(Member cloned) {
        this.user = cloned.getUser();
        this.familyTree = cloned.getFamilyTree();
        this.info = cloned.getInfo();
        this.parent = cloned.getParent();
        this.children = cloned.getChildren();
        this.ancestry = cloned.getAncestry();
    }

    /**
     * Method for checking if member has parent. It is not necessary for parent field to be different from null if ancestry says that parent exists.
     * Checking is done on ancestry field only.
     *
     * @return true if member has parent, false otherwise
     */
    public boolean hasParent() {
        return ancestry.split("/").length - 1 > 0;
    }

    /**
     * Method for checking if member has children
     *
     * @return true if member has children, false otherwise
     */
    public boolean hasChildren() {
        return getChildren() != null && !children.isEmpty();
    }

    public boolean hasMoreThanOneChild() {
        return getChildren() != null && !children.isEmpty() && children.size() > 1;
    }

    public Member addChild(Member child) {
        if (child != null) {
            if (children == null) {
                children = new ArrayList<>(List.of(child));
            } else children.add(child);
            child.setParent(this);
        }
        return this;
    }

    public Member removeChild(Member child) {
        if (child != null && children != null) {
            child.setParent(null);
            children.remove(child);
        }
        return this;
    }

}
