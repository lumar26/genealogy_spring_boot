package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyResponse {
    private String name;
    private MemberBasicInfoResponse basicInfo;
    private List<HierarchyResponse> children;

    public void addChild(HierarchyResponse child) {
        if (!children.contains(child))
            children.add(child);
    }
}
