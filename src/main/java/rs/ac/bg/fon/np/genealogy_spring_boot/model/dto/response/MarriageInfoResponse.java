package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MarriageInfoResponse {
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private Long husbandId;
    private Long wifeId;
    private String husband;
    private String wife;
    private String husbandFamilyTree;
    private String wifeFamilyTree;

    public static MarriageInfoResponse of(Marriage entity) {
        return new MarriageInfoResponse(entity.getId(), entity.getStartDate(), entity.getEndDate(),
                entity.getHusband().getId(), entity.getWife().getId(),
                entity.getHusband().getInfo().getName() + " " + entity.getHusband().getInfo().getSurname(),
                entity.getWife().getInfo().getName() + " " + entity.getWife().getInfo().getSurname(),
                entity.getHusband().getFamilyTree().getTitle(),
                entity.getWife().getFamilyTree().getTitle());
    }
}
