package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.LocalDate;

/**
 * Embedded object in Member entity, contains all basic information that describes member of family tree as a person.
 *
 * @author lumar26
 * @version 1.0
 */
@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MemberInfo {

    /**
     * Inner enum for representing gender of member
     */
    public enum Gender {
        MALE, FEMALE;

        public Gender toOpposite() {
            if (this.equals(MALE)) return FEMALE;
            return MALE;
        }
        }

    /**
     * Member's first name
     */
    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    /**
     * Member's last name
     */
    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String surname;

    /**
     * Member's gender, either MALE or FEMALE
     */
    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private Gender gender;

    /**
     * Member's birth date, must be in the past
     */
    @Column(name = "birth_date", nullable = false)
    @EqualsAndHashCode.Include
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    /**
     * Member's death date, must be in the past
     */
    @Column(name = "death_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deathDate;

    /**
     * Place of birth of this member
     */
    @Column(name = "birth_place")
    @EqualsAndHashCode.Include
    private String birthPlace;

    /**
     * Place of death of this member
     */
    @Column(name = "death_place")
    private String deathPlace;

    /**
     * Address where member currently lives.
     */
    @Embedded
    private Address currentAddress;

}
