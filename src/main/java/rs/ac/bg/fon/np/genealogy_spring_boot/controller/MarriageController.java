package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MarriageSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MarriageService;

/**
 * Spring boot RESTful controller class that exposes endpoints for manipulating marriage between two members in family tree, including CRUD operations and others
 * This class has references to service layer classes that contain business logic for marriages.
 *
 * @author lumar26
 * @version 1.0
 */
@RestController
@RequestMapping("api/marriage")
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class MarriageController {

    /**
     * Spring boot Service class for operating with marriages
     */
    private final MarriageService marriageService;

    @Autowired
    public MarriageController(MarriageService marriageService) {
        this.marriageService = marriageService;
    }

    /**
     * Api Endpoint for saving new marriage of two members into database
     *
     * @param request new member to be saved, request payload
     * @return saved marriage
     */
    @PostMapping()
    public ResponseEntity<?> addMarriage(@RequestBody MarriageSaveRequest request) {
        try {
            return ResponseEntity.ok(marriageService.saveMarriage(request));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not save new marriage. " + e.getMessage());
        }
    }

    @GetMapping("/member/{memberId}")
    public ResponseEntity<?> getMarriageOfMember(@PathVariable("memberId") Long memberId) {
        try {
            return ResponseEntity.ok(marriageService.getMarriageOfMember(memberId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Marriage not found. " + e.getMessage());
        }
    }

    @GetMapping("/potential_spouses/{id}")
    public ResponseEntity<?> getPotentialSpouses(@PathVariable("id") Long memberId) {
        try {
            return ResponseEntity.ok(marriageService.getPotentialSpouses(memberId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not save new marriage. " + e.getMessage());
        }
    }

    @GetMapping("/{id}/spouse")
    public ResponseEntity<?> getSpouse(@PathVariable("id") Long memberId) {
        try {
            return ResponseEntity.ok(marriageService.getSpouse(memberId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Spouse not found. " + e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMarriage(@PathVariable("id") Long marriageId) {
        try {
            return ResponseEntity.ok(marriageService.deleteMarriageById(marriageId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not delete marriage with id = " + marriageId + ". " + e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> terminateMarriage(@PathVariable("id") Long marriageId) {
        try {
            return ResponseEntity.ok(marriageService.terminateMarriage(marriageId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not delete marriage with id = " + marriageId + ". " + e.getMessage());
        }
    }


}
