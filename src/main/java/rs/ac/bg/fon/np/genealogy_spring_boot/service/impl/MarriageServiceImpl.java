package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MarriageSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MarriageInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MarriageRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MemberRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MarriageService;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MarriageServiceImpl implements MarriageService {
    private final MarriageRepository repository;
    private final MemberRepository memberRepository;

    @Autowired
    public MarriageServiceImpl(MarriageRepository repository, MemberRepository memberRepository) {
        this.repository = repository;
        this.memberRepository = memberRepository;
    }

    private void checkIfSpousesAreReserved(Member husband, Member wife) {
        List<Marriage> existingMarriages = repository.findAllByHusbandEqualsOrWifeEquals(husband, wife);
        if (!existingMarriages.isEmpty())
            for (Marriage m : existingMarriages)
                if (m.isActive())
                    throw new IllegalStateException("One of the members is already married. Marriage: " + m);
    }

    private void checkAndAdjustGender(Marriage marriage) {
        if ((marriage.getHusband().getInfo().getGender().equals(MemberInfo.Gender.MALE) && marriage.getWife().getInfo().getGender().equals(MemberInfo.Gender.MALE)) ||
                (marriage.getHusband().getInfo().getGender().equals(MemberInfo.Gender.FEMALE) && marriage.getWife().getInfo().getGender().equals(MemberInfo.Gender.FEMALE)))
            throw new IllegalStateException("Spouses cannot be of same gender");

//            if husband and wife are accidentally swapped
        if (marriage.getHusband().getInfo().getGender().equals(MemberInfo.Gender.FEMALE)) {
            Member temp = marriage.getHusband();
            marriage.setHusband(marriage.getWife());
            marriage.setWife(temp);
        }

    }

    @Override
    public MarriageInfoResponse saveMarriage(MarriageSaveRequest req) throws Exception {
        Member husband = memberRepository.findById(req.getHusbandId())
                .orElseThrow(() -> new Exception("Husband with id = " + req.getHusbandId() + " does not exist."));
        Member wife = memberRepository.findById(req.getWifeId())
                .orElseThrow(() -> new Exception("Wife with id = " + req.getWifeId() + " does not exist."));
        if (req.getStartDate() == null || req.getStartDate().isAfter(LocalDate.now()))
            req.setStartDate(LocalDate.now());

        Marriage marriage = new Marriage(req.getStartDate(), husband, wife);
        /*---------- MARRIAGE VALIDATION --------------*/
        checkIfSpousesAreReserved(husband, wife);
        checkAndAdjustGender(marriage);
        /*---------- MARRIAGE VALIDATION --------------*/

        return MarriageInfoResponse.of(repository.save(marriage));
    }

    @Override
    public MemberBasicInfoResponse getSpouse(Long subjectId) throws Exception {
        Marriage marriage = repository.findMarriageBySingleSpouse(subjectId).orElseThrow(() -> new Exception("There is no marriage for member with id = " + subjectId));
//        if marriage exist the participant other than subject is returned
        if (Objects.equals(marriage.getHusband().getId(), subjectId))
            return MemberBasicInfoResponse.of(marriage.getWife());
        if (Objects.equals(marriage.getWife().getId(), subjectId))
            return MemberBasicInfoResponse.of(marriage.getHusband());
        return null;
    }

    @Override
    public List<MemberBasicInfoResponse> getPotentialSpouses(Long memberId) throws Exception {
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new Exception("Member with id = " + memberId + " not found"));

//        у зависности од пола бирамо или мушкарце или жене
        List<Long> marriedIds = member.getInfo().getGender().equals(MemberInfo.Gender.MALE)
                ? repository.findActiveWifeIds()
                : repository.findActiveHusbandIds();

        List<Member> allSingles;
        if (marriedIds.isEmpty())
            allSingles = memberRepository.findAll();
        else
            allSingles = memberRepository.findAllByIdNotIn(marriedIds);

        List<Member> potentialSpouses = allSingles
                .stream()
//                у случају да су из истог стабла или истог пола не могу ступити у брак
                .filter(potentialSpouse -> !(potentialSpouse.getFamilyTree().equals(member.getFamilyTree()) || potentialSpouse.getInfo().getGender().equals(member.getInfo().getGender()))) // изузимамо чланове стабла у коме се налази члан за кога враћамо потенцијалне супружнике
                .collect(Collectors.toList());

        return potentialSpouses.stream().map(MemberBasicInfoResponse::of).collect(Collectors.toList());
    }

    @Override
    public MarriageInfoResponse terminateMarriage(Long marriageId) throws Exception {
        Marriage marriage = repository.findById(marriageId).orElseThrow(() -> new Exception("Marriage with id = " + marriageId + " does not exist"));
        marriage.setEndDate(LocalDate.now());
        repository.save(marriage);
        return MarriageInfoResponse.of(marriage);
    }

    @Override
    public MarriageInfoResponse getMarriageOfMember(Long memberId) throws Exception {
        Member member = memberRepository.findById(memberId).orElseThrow(() -> new Exception("Member with id = " + memberId + " not found."));
        Marriage marriage = repository.findMarriageByHusband_IdOrWife_Id(member.getId(), member.getId())
                .orElseThrow(() -> new Exception("Marriage for member " + member.getInfo().getName() + " not found."));
        if (marriage.getEndDate() != null && !marriage.getEndDate().isAfter(LocalDate.now()))
            throw new Exception("Marriage for member " + member.getInfo().getName() + " not active.");
        return MarriageInfoResponse.of(marriage);
    }

    @Override
    public MarriageInfoResponse deleteMarriageById(long id) {
        if (id < 1) throw new IllegalStateException("Could not delete marriage: invalid marriage id");
        Optional<Marriage> optionalMarriage = repository.findById(id);
        if (optionalMarriage.isEmpty())
            throw new IllegalStateException("Could not delete marriage, id = " + id + ": marriage does not exist");
        repository.delete(optionalMarriage.get());
        return MarriageInfoResponse.of(optionalMarriage.get());
    }
}
