package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.UserLoginRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.UserRegisterRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.AuthenticationResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.RefreshToken;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.RegistrationVerificationToken;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.RefreshTokenRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.RegistrationVerificationTokenRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.security.util.JwtUtil;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Class that contains logic for authenticating users as well as retrieving users from database and other operations on existing users.
 *
 * @author lumar26
 * @version 1.0
 */
@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    /**
     * JPA repository responsible for interacting with database.
     */
    private final UserRepository repository;
    /**
     * Predefined password encoder responsible for encoding user password.
     */
    private final PasswordEncoder passwordEncoder;
    private final RegistrationVerificationTokenRepository tokenRepository;
    private final RefreshTokenRepository refreshTokenRepository;
    private final JwtUtil jwtUtil;

    /**
     * Service method that contains logic for registering new user, i.e. saving him into database. Method checks if user already exists, based on username and email.
     *
     * @param accountDto dto object passed down from controller which contains all credentials for new user except his role.
     * @return instance of {@link User} entity, newly saved user in database.
     * @throws Exception in case there already exists user with provided username or email.
     */
    public User registerUser(UserRegisterRequest accountDto) throws Exception {
        if (accountDto == null) throw new IllegalStateException("Could not register user. Invalid request object.");
        Optional<User> found = repository.findUserByUsernameOrEmail(accountDto.getUsername(), accountDto.getEmail());
        if (found.isPresent()) {
            throw new Exception("There is already an account with email address: " + accountDto.getEmail());
        }
        User user = new User(
                accountDto.getName(),
                accountDto.getSurname(),
                accountDto.getEmail(),
                accountDto.getUsername(),
                passwordEncoder.encode(accountDto.getPassword()),
                User.Role.ROLE_USER);

        return repository.save(user);
    }

    /**
     * Retrieves from database user with provided id.
     *
     * @param id unique identifier for user.
     * @return instance of {@link User} entity that exists in database.
     * @throws IllegalStateException if invalid id is passed (e.g. id < 1) or user with given id does not exist in database.
     */
    public User getUserById(Long id) throws Exception {
        if (id == null || id <= 0) throw new Exception("Could not find user: invalid id.");
        Optional<User> dbUser = repository.findById(id);
        if (dbUser.isEmpty()) throw new Exception("Could not find user: user does not exist in database.");
        return dbUser.get();
    }

    /**
     * Retrieves from database user with provided id.
     *
     * @param username user's username.
     * @return instance of {@link User} entity that exists in database.
     * @throws UsernameNotFoundException if there is no such username in database.
     */
    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.getByUsername(username);
    }

    public void createVerificationToken(User user, String token) {
        final RegistrationVerificationToken myToken = new RegistrationVerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    public User getUserByVerificationToken(String token) {
        return tokenRepository.findByToken(token).getUser();
    }

    public RegistrationVerificationToken getVerificationToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public void enableUserAccount(User user) {
        user.setEnabled(true);
        repository.save(user);
    }

    public AuthenticationResponse enablePremiumAccess(Long userId) throws Exception {
        final User user = repository.findById(userId).orElseThrow(() -> new NotFoundException("User with id = " +  userId + " not found."));
        user.setRole(User.Role.ROLE_PREMIUM_USER);
        final User saved = repository.save(user);
        return new AuthenticationResponse(user.getId(),
                user.getUsername(), jwtUtil.generateToken(user), createRefreshToken(user), user.getRole());
    }

    public AuthenticationResponse refreshAccessToken(String token) throws Exception {
        RefreshToken refreshToken = refreshTokenRepository.findByToken(token).orElseThrow(() -> new Exception("Refresh token does not exist."));
        String newRefreshToken = createRefreshToken(refreshToken.getUser());
        String newJwt = jwtUtil.generateToken(refreshToken.getUser());
        System.out.println("\nToken (ends with): " + newJwt.substring(newJwt.length() - 5) + "\ncreated (refreshToken) at " + LocalDateTime.now() + "\nexpires at: " + jwtUtil.extractExpiration(newJwt) + "\n");
        return new AuthenticationResponse(
                refreshToken.getUser().getId(),
                refreshToken.getUser().getUsername(),
                newJwt,
                newRefreshToken,
                refreshToken.getUser().getRole()
        );
    }

    @Transactional
    public AuthenticationResponse authenticate(UserLoginRequest loginRequest) {
        final User user = repository.getByUsername(loginRequest.getUsername());
        final String jwt = jwtUtil.generateToken(user);
        System.out.println("\nToken (ends with): " + jwt.substring(jwt.length() - 5) + "\ncreated (authenticate) at " + LocalDateTime.now() + "\nexpires at: " + jwtUtil.extractExpiration(jwt) + "\n");

        final String refreshToken = createRefreshToken(user); // refresh token rotation
        return new AuthenticationResponse(user.getId(), user.getUsername(), jwt, refreshToken, user.getRole());
    }

    private String createRefreshToken(User user) {
        String token = jwtUtil.generateRefreshToken();
        var refreshTokenByUser = refreshTokenRepository.findRefreshTokenByUser(user);
        if (refreshTokenByUser.isPresent()) {
            // in case there is already token for given user
            RefreshToken existent = refreshTokenByUser.get();
            existent.setToken(token);
            existent.setExpiration(LocalDate.now().plusMonths(6));
            refreshTokenRepository.save(existent);
            return existent.getToken();
        }
        RefreshToken refreshToken = new RefreshToken()
                .setToken(token)
                .setUser(user)
                .setExpiration(LocalDate.now().plusMonths(6));

        RefreshToken saved = refreshTokenRepository.save(refreshToken);
        return saved.getToken();
    }
}
