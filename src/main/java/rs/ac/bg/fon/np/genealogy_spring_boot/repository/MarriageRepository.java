package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/**
 * Interface that extends JpaRepository which provides all methods for basic CRUD operations, specialized for working with Marriage entity.
 *
 * @author lumar26
 * @version 1.0
 */
@Repository
public interface MarriageRepository extends JpaRepository<Marriage, Long> {
    /**
     * Method for retrieving all marriages where either husband is provided husband object or wife is provided wife object.
     *
     * @param husband Husband in marriage
     * @param wife Wife in marriage
     * @return List of Marriages with either husband or wife
     */
    List<Marriage> findAllByHusbandEqualsOrWifeEquals(Member husband, Member wife);

    /**
     * Method for retrieving all marriages where either husband has provided husband id or wife has provided wife's id.
     *
     * @param idAsHusband Husband's id
     * @param idAsWife Wife's id
     * @return Marriage entity
     */
    Optional<Marriage> findMarriageByHusband_IdOrWife_Id(long idAsHusband, long idAsWife);

    @Query("select m from Marriage m where m.husband.id = :subjectId or m.wife.id = :subjectId")
    Optional<Marriage> findMarriageBySingleSpouse(long subjectId);

    @Query("select m.husband.id from Marriage m where m.endDate is null")
    List<Long> findActiveHusbandIds();

    @Query("select m.wife.id from Marriage m where m.endDate is null")
    List<Long> findActiveWifeIds();

}
