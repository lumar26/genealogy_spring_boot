package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.RefreshToken;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, String> {
    Optional<RefreshToken> findRefreshTokenByUser(User user);

    Optional<RefreshToken> findByToken(String token);
}
