package rs.ac.bg.fon.np.genealogy_spring_boot.validation;

import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberInfoResponse;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MemberInfoDtoValidator implements ConstraintValidator<ValidMemberDto, MemberInfoResponse> {
    @Override
    public boolean isValid(MemberInfoResponse dto, ConstraintValidatorContext constraintValidatorContext) {
        return  (dto.getGender().equals("MALE") || dto.getGender().equals("FEMALE"))
                && (dto.getDeathDate() == null || dto.getDeathDate().isAfter(dto.getBirthDate()));
    }
}
