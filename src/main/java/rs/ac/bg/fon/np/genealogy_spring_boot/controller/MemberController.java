package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberInfoUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.HierarchyResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MemberService;

import javax.validation.Valid;
import java.util.List;

/**
 * Spring boot RESTful controller class that exposes endpoints for manipulating members inside family tree, including CRUD operations and others
 * This class has references to service layer classes that contain business logic for members.
 *
 * @author lumar26
 * @version 1.0
 */
@RestController
@RequestMapping("/api/member")
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.PUT, RequestMethod.DELETE})
public class MemberController {
    /**
     * Spring boot Service class for operating with members
     */
    private final MemberService service;

    @Autowired
    public MemberController(MemberService service) {
        this.service = service;
    }

    /**
     * Api Endpoint for saving new member into database
     *
     * @param dto new member to be saved, request payload
     * @return saved member
     */
    @PostMapping()
    public ResponseEntity<?> saveMember(@RequestBody @Valid MemberSaveRequest dto) {
        try {
            MemberBasicInfoResponse saved = service.saveMember(dto);
            return ResponseEntity.ok(saved);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not save new member in family tree." + e.getMessage());
        }
    }

    /**
     * Api endpoint for finding and retrieving member from database based on passed identifier from client side
     *
     * @param id unique identifier for member in database, search criteria
     * @return found member from database
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getMemberDetailsById(@PathVariable("id") Long id) {
        try {
            MemberInfoResponse member = service.getMemberById(id);
            return ResponseEntity.ok(member);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not retrieve member with id = " + id + ". " + e.getMessage());
        }
    }

    /**
     * Method for retrieving all members of given family tree, that are currently stored in database
     *
     * @return list of family tree members
     */
    @GetMapping("/family_tree/{familyTree}")
    public ResponseEntity<?> getMembersOfFamilyTree(@PathVariable("familyTree") Long familyTreeId) {
        try {
            List<MemberBasicInfoResponse> members = service.getMembersInFamilyTree(familyTreeId);
            return ResponseEntity.ok(members);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not retrieve members of family tree with id = " + familyTreeId + ". " + e.getMessage());
        }
    }

    /**
     * Method for retrieving all members of given family tree as tree structure, that are currently stored in database
     *
     * @return hierarchy of family tree members | Error message with status code 400
     */
    @GetMapping("/hierarchy/{familyTree}")
    public ResponseEntity<?> getMembersOfFamilyTreeAsHierarchy(@PathVariable("familyTree") Long familyTreeId) {
        try {
            HierarchyResponse hierarchy = service.getMembersHierarchy(familyTreeId);
            return ResponseEntity.ok(hierarchy);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not retrieve hierarchy of family tree with id = " + familyTreeId + ". " + e.getMessage());
        }
    }

    /**
     * Api endpoint for finding and updating member's information data with new data passed as payload.
     * This endpoint does not handle updating relationships with other members, nor it changes member's family tree or owner user.
     *
     * @param id  unique identifier for member in database, search criteria
     * @param dto new data for information about member, request payload
     * @return updated member from database
     */
    @PutMapping("/info/{id}")
    public ResponseEntity<?> updateMemberInfo(@PathVariable("id") Long id,
                                              @RequestBody @Valid MemberInfoUpdateRequest dto) {
        try {
            MemberInfoResponse response = service.updateMemberInfo(id, dto);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not update member info for member with id = " + id + ". " + e.getMessage());
        }
    }

    /**
     * Api endpoint for deleting member based on passed id from client side
     *
     * @param id unique identifier for member in database, search and delete criteria
     * @return deleted member
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMemberById(@PathVariable("id") Long id) {
        try {
            MemberBasicInfoResponse deleted = service.deleteMemberById(id, true);
            return ResponseEntity.ok(deleted);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not delete member with id = " + id + ". " + e.getMessage());
        }
    }

    @GetMapping("/kinship/{first}/{second}")
    public ResponseEntity<?> getKinshipOfMembers(@PathVariable("first") Long firstMemberId,
                                                 @PathVariable("second") Long secondMemberId){
        try {
            return ResponseEntity.ok(service.findKinshipBetweenMembers(firstMemberId, secondMemberId));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Could not determine kinship. " + e.getMessage());
        }
    }


}
