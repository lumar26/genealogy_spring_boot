package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import org.springframework.stereotype.Service;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MarriageSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MarriageInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;

import java.util.List;

/**
 * Spring boot RESTful service class that contains business logic for marriages.
 * This includes CRUD operations and some other domain-specific operations with marriages
 *
 * @author lumar26
 * @version 1.0
 */
@Service
public interface MarriageService {
    /**
     * Method that returns marriage of passed member, if member is married.
     * Passed member can be either wife or husband, no difference made.
     *
     * @param memberId id of a member whose marriage should be retrieved, search criteria
     * @return members' marriage
     * @throws IllegalStateException if member is null
     */
    MarriageInfoResponse getMarriageOfMember(Long memberId) throws Exception;

    /**
     * Deletes marriage based on given id.
     *
     * @param id unique identifier for marriagein database, delete criteria
     * @return deleted marriage
     */
    MarriageInfoResponse deleteMarriageById(long id);


    /**
     * Method for saving new marriage.
     * Method checks if spouses are entered validly, must be of different gender, must be alive in same time.
     * Method also checks if members are already married
     *
     * @param newMarriage new marriage data to be saved
     * @return saved marriage
     * @throws Exception if one of spouses (or both) is already married; if spouses are of same gender.
     */
    MarriageInfoResponse saveMarriage(MarriageSaveRequest newMarriage) throws Exception;

    MemberBasicInfoResponse getSpouse(Long subjectId) throws Exception;
    List<MemberBasicInfoResponse> getPotentialSpouses(Long memberId) throws Exception;

    MarriageInfoResponse terminateMarriage(Long marriageId) throws Exception;
}
