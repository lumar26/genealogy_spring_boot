package rs.ac.bg.fon.np.genealogy_spring_boot.validation;

import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MemberSaveDtoValidator implements ConstraintValidator<ValidMemberDto, MemberSaveRequest> {
    @Override
    public boolean isValid(MemberSaveRequest dto, ConstraintValidatorContext constraintValidatorContext) {
        return  (dto.getGender().equals("MALE") || dto.getGender().equals("FEMALE"))
                && (dto.getDeathDate() == null || dto.getDeathDate().isAfter(dto.getBirthDate()));
    }
}
