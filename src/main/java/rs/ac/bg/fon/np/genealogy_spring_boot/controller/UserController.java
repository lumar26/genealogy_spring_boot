package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import rs.ac.bg.fon.np.genealogy_spring_boot.events.event.RegistrationCompleteEvent;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.RefreshTokenRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.UserLoginRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.UserRegisterRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.AuthenticationResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.RegistrationVerificationToken;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.UserService;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Spring boot RESTful controller class that exposes endpoints responsible for user authentication and authorization.
 *
 * @author lumar26
 * @version 1.0
 */
@RestController
@RequestMapping("/auth")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.PUT, RequestMethod.PATCH})
public class UserController {
    /**
     * This is dependency of service layer class {@link UserService} which contains logic for authenticating user.
     */
    private final UserService userService;

    private final ApplicationEventPublisher publisher;

    private final AuthenticationManager manager;

    /**
     * Endpoint for registering new user. This user by default has role: ROLE_USER of type {@link User.Role}. Users with role ROLE_ADMIN are predefined in system. Does not contain any validation considering password strength. Username and email must be unique.
     *
     * @param registerRequest request body that contains all information necessary to create new record of user in system.
     * @return {@link ResponseEntity} response with either status 200 and body that contains necessary credentials of authenticated user in form of {@link AuthenticationResponse}, or response with error code 500 if registration failed.
     */
    @PostMapping("/register")
    @Transactional
    public ResponseEntity<?> registerUser(@RequestBody UserRegisterRequest registerRequest) {
        User user;
        try {
            user = userService.registerUser(registerRequest);
            publisher.publishEvent(new RegistrationCompleteEvent(user)); // this event is synchronous
            return ResponseEntity.ok("Email verification required"); // todo: change return data
//            return ResponseEntity.ok(new AuthenticationResponse(user.getId(), user.getUsername(), jwtUtil.generateToken(user), user.getRole()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    /**
     * Endpoint for confirming registration process via email. When newly registered user clicks on a link sent by email he is redirected to <i>/registration/verify</i> path. When user accesses this address a GET request is automatically sent with the token recieved in email.
     *
     * @param token token sent via email to user. This token is used to retrieve registered user from database.
     * @return {@link ResponseEntity} response with either status 200 and body that contains necessary credentials of authenticated user in form of {@link AuthenticationResponse}, or response with error code 500 if registration failed.
     */
    @GetMapping("/registration/verify")
    public ResponseEntity<?> verifyRegistration(@RequestParam("token") String token) {
        RegistrationVerificationToken existingToken = userService.getVerificationToken(token);
//        todo: use Optional<>
        if (existingToken == null)
            return ResponseEntity.badRequest().body("Invalid verification token");
        if (existingToken.getExpirationDateTime().isBefore(LocalDateTime.now()))
            return ResponseEntity.badRequest().body("Verification code expired at: " + existingToken.getExpirationDateTime());

        User user = existingToken.getUser();
        userService.enableUserAccount(user);


        File htmlFile = new File("src/main/resources/static/account_activation.html");
        StringBuilder html = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(htmlFile));
            String line;
            while ((line = in.readLine()) != null) {
                html.append(line);
                html.append("\n");
            }
            System.out.println("Read html file");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body("Could not find html page to render");
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);

        return ResponseEntity.ok().headers(responseHeaders).body(html);
    }


    /**
     * Endpoint for signing in user based on his username and password.
     *
     * @param loginRequest request payload of type {@link UserLoginRequest} which contains username and password
     * @return {@link ResponseEntity} response with either status 200 and body that contains necessary credentials of authenticated user in form of {@link AuthenticationResponse} as well as refresh token in response header, or response with error code 403 if login failed.
     */
    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody UserLoginRequest loginRequest) {
        //        autentifikacija standardnog tokena za username i password
        try {
            manager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())); // throws runtime exception
            return ResponseEntity.ok(userService.authenticate(loginRequest));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Could not authenticate user with given credentials. " + e.getMessage());
        }
    }

    /**
     * After access token expires client sends request for refreshing access token. Method checks if refresh token is still valid, and if it is it creates new access token based on username passed in request, and also creates new refresh token
     *
     * @param request String value representing refresh token
     * @return {@link ResponseEntity} containing either {@link AuthenticationResponse} with new JWT token and new refresh cookie in response header
     */
    @PostMapping("/refresh")
    public ResponseEntity<?> refreshAccessToken(@RequestBody RefreshTokenRequest request) {
        try {
            return ResponseEntity.ok(userService.refreshAccessToken(request.getRefreshToken()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not refresh access token. " + e.getMessage());
        }
    }

    @PatchMapping("enable_premium/{id}")
    public ResponseEntity<?> refreshAccessToken(@PathVariable("id") Long userId) {
        try {
            return ResponseEntity.ok(userService.enablePremiumAccess(userId));
        } catch (NotFoundException nfe) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not enable premium. " + nfe.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Could not enable premium. " + e.getMessage());
        }
    }
}
