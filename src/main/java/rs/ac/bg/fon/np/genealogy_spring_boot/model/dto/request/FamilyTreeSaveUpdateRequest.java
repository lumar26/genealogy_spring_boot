package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DTO representing payload for UPDATE and SAVE request for family tree. All fields in thi dto must meet certain preconditions in terms of content.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class FamilyTreeSaveUpdateRequest {
    @NotBlank(message = "Family tree title must be provided")
    private String title;
    private String description;
    @NotNull(message = "User's (creator of the tree) id must be provided")
    @Min(value = 1, message = "user's (creator of the tree) id must be positive integer")
    private Long userId;
}
