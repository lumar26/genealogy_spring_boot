package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Address;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.validation.ValidMemberDto;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

/**
 * DTO representing request for persisting new member to database..
 * This DTO should contain info necessary for new member to be saved such as id of user who had created it,id of family tree, name, surname, gender, date of birth etc.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidMemberDto
public class MemberSaveRequest {
    @NotNull(message = "User's id must be specified")
    @Min(value = 1, message = "User's id must be positive integer")
    private Long userId;

    @NotNull(message = "Family tree id must be specified")
    @Min(value = 1, message = "Family tree id must be positive integer")
    private Long familyTreeId;

    //    handled by custom validator
    private Long parentId;

    private Long childId;

    @NotBlank(message = "Name of new member must be defined")
    private String name;

    @NotBlank(message = "Gender of new member must be defined")
    private String gender;

    //    handled by custom validator
    @NotBlank(message = "Surname of new member must be defined")
    private String surname;

    @NotNull(message = "Date of birth for a new member must be defined")
    @Past(message = "Date of birth must be in the past")
    private LocalDate birthDate;

    @Past(message = "Date of birth must be in the past")
    private LocalDate deathDate;

    private String deathPlace;
    private String birthPlace;

    private String country;
    private String postalCode;
    private String cityOrMunicipality;
    private String street;
    private String number;
}
