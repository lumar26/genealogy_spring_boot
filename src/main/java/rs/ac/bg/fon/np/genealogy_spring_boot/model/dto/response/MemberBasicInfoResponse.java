package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * DTO representing response that contains basic info about member, such as id, his name, surname and family tree he/she belongs to.
 * This DTO is used as response after CRUD operations on member that do not require detailed info about member, but only as assertion that operation was successfully executed.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberBasicInfoResponse {
    @NotNull
    @Min(value = 1)
    private Long id;
    @NotBlank
    private String ancestry;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    private String familyTree;
    private Long familyTreeId;
    private Long ownerId;

    public static MemberBasicInfoResponse of(Member entity) {
        return new MemberBasicInfoResponse(
                entity.getId(),
                entity.getAncestry(),
                entity.getInfo().getName(),
                entity.getInfo().getSurname(),
                entity.getFamilyTree().getTitle(),
                entity.getFamilyTree().getId(),
                entity.getUser().getId());
    }
}
