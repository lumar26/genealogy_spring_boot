package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Address;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.validation.ValidMemberDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@ValidMemberDto
@Accessors(chain = true)
public class MemberInfoUpdateRequest {
    @NotBlank(message = "Member's name must be defined")
    private String name;
    @NotBlank(message = "Member's surname must be defined")
    private String surname;
    @NotBlank(message = "Member's gender must be defined")
    private String gender;
    @NotNull(message = "Member's date of birth must be defined")
    private LocalDate birthDate;
    private LocalDate deathDate;
    private String birthPlace;
    private String deathPlace;
    private String country;
    private String postalCode;
    private String cityOrMunicipality;
    private String street;
    private String number;

    public MemberInfo toEntity(){
        Address address = new Address(country, postalCode, cityOrMunicipality, street, number);
        return new MemberInfo(name, surname, MemberInfo.Gender.valueOf(gender), birthDate, deathDate, birthPlace, deathPlace, address);
    }
}
