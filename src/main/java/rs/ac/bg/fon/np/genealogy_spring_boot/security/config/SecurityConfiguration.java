package rs.ac.bg.fon.np.genealogy_spring_boot.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import rs.ac.bg.fon.np.genealogy_spring_boot.security.filter.JwtRequestFilter;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.UserService;

/**
 * Configuration class that contains basic setup and configuration for Spring Security: token based authentication with JWT tokens.
 *
 * @author lumar26
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private final JwtRequestFilter jwtRequestFilter;

    @Autowired
    public SecurityConfiguration(UserService userService, JwtRequestFilter jwtRequestFilter) {
        this.userService = userService;
        this.jwtRequestFilter = jwtRequestFilter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        configuring cors and csrf
        httpSecurity.cors().and().csrf().disable();

//        defining authorization
        httpSecurity
                .authorizeRequests()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/swagger-ui/**", "/v3/swagger-ui/**", "/v3/api-docs/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/**").access("hasRole('ROLE_USER') || hasRole('ROLE_PREMIUM_USER')")
                .antMatchers(HttpMethod.POST, "/api/**").access("hasRole('ROLE_USER') || hasRole('ROLE_PREMIUM_USER')")
                .antMatchers(HttpMethod.PUT, "/api/**").access("hasRole('ROLE_USER') || hasRole('ROLE_PREMIUM_USER')")
                .antMatchers(HttpMethod.PATCH, "/api/**").access("hasRole('ROLE_USER') || hasRole('ROLE_PREMIUM_USER')")
                .anyRequest().authenticated();

        httpSecurity.exceptionHandling();

//        set session management to stateless since we are building REST api
        httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

//        adding filter for jwt
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }
}
