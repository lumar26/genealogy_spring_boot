package rs.ac.bg.fon.np.genealogy_spring_boot.security.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

/**
 * Util class for operations with JWT token e.g. creating and generating it, checking its validity, extracting info form token etc.
 *
 * @author lumar26
 * @version 1.0
 */
@NoArgsConstructor
public class JwtUtil {
    private final String SECRET_KEY = "secret";
    private static final int JWT_EXPIRATION = 1000 * 60 * 15; // 15 min todo: extract to app.properties

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

    public String generateRefreshToken() {
//        return Generator.generateRefreshToken(REFRESH_LENGTH);
        return UUID.randomUUID().toString();
    }

    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_EXPIRATION))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        try {
            extractExpiration(token); // throws ExpiredJWTException which is Runtime exception if access token has expired
        } catch (ExpiredJwtException e) {
            System.err.println("JWT " + token.substring(token.length() - 5) + " expired: " + e.getMessage());
            return false;
        }
//        if (LocalDateTime.now().isAfter(LocalDateTime.of(extractExpiration(token))))
        final String username = extractUsername(token);
        boolean result = (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
        return result;
    }

}
