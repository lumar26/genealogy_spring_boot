package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

/**
 * DTO that represents form of response sent to client app on successful registration or login.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class AuthenticationResponse {
    private Long userId;
    private String username;
    private String jwt;
    private String refreshToken;
    private User.Role role;
}
