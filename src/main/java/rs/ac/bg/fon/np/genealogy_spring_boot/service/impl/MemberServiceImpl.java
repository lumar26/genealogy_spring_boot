package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.np.genealogy_spring_boot.kinship.KinshipCalculator;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberInfoUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.HierarchyResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.KinshipResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.FamilyTreeRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MarriageRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MemberRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MemberService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class MemberServiceImpl implements MemberService {
    private final MemberRepository repository;
    private final UserRepository userRepository;
    private final FamilyTreeRepository familyTreeRepository;
    private final MarriageRepository marriageRepository;


    @Autowired
    public MemberServiceImpl(MemberRepository repository, UserRepository userRepository, FamilyTreeRepository familyTreeRepository, MarriageRepository marriageRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.familyTreeRepository = familyTreeRepository;
        this.marriageRepository = marriageRepository;
    }


    @Transactional
    @Override
    public MemberBasicInfoResponse saveMember(MemberSaveRequest req) throws Exception {
        if (req == null) throw new Exception("Invalid request for saving member.");

//      setting spouse, parent and children
        Member member = populateMember(req);


//        provera da li je novi član povezan sa nekim drugim članom, ukoliko stablo nije prazno
//        najpre proveravamo ima li već članova u stablu
        List<Member> membersOfTree = repository.findAllByFamilyTree(member.getFamilyTree());
        if (!membersOfTree.isEmpty() && // ima članova u stablu
                member.getParent() == null && // član nema roditelja
                (member.getChildren() == null || member.getChildren().isEmpty())) // clan nema dece
            throw new Exception("Member must be in relationship with one of the existing members of given tree.");


//        potrebno je da se prvo član sačuva u bazi kako bi dobio svoj id
        Member saved = repository.save(member);
//        saved mora da ima primarni ključ kako bi polje 'ancestry' kod dece moglo da se ažurira
        updateChildrenAncestries(saved);
        return MemberBasicInfoResponse.of(saved);
    }

    private Member populateMember(MemberSaveRequest r) throws Exception {
        Member parent = null;
        FamilyTree familyTree;
        User user;
        Member child = null;

        if (r.getFamilyTreeId() == null) throw new Exception("Family tree id in request cannot be null.");
        if (r.getUserId() == null) throw new Exception("User id in request cannot be null.");

        familyTree = familyTreeRepository.findById(r.getFamilyTreeId()).orElseThrow(() -> new Exception("Invalid family tree of member: tree does not exist."));

        user = userRepository.findById(r.getUserId()).orElseThrow(() -> new Exception("Invalid user of member: user does not exist."));

        if (r.getParentId() != null)
            parent = repository.findById(r.getParentId()).orElseThrow(() -> new Exception("Invalid parent id passed: " + r.getParentId()));

        if (r.getChildId() != null) {
            child = repository.findById(r.getChildId()).orElseThrow(() -> new Exception("Invalid child id passed: " + r.getChildId()));
//                    провера да ли дете можда већ има родитеља
            if (child.hasParent()) throw new Exception("Child of new member already has parent");
        }

        Address address = new Address(r.getCountry(), r.getPostalCode(), r.getCityOrMunicipality(), r.getStreet(), r.getNumber());
        MemberInfo info = new MemberInfo(r.getName(), r.getSurname(), MemberInfo.Gender.valueOf(r.getGender()), r.getBirthDate(), r.getDeathDate(), r.getBirthPlace(), r.getDeathPlace(), address);

        return new Member().setInfo(info).setParent(parent).addChild(child).setFamilyTree(familyTree).setUser(user).setAncestry(parent != null ? "/" + parent.getId() + parent.getAncestry() : "/"); // if parent is defined than handle ancestry
    }

    private void updateChildrenAncestries(Member member) {
        if (!member.hasChildren()) return;
        // update ancestry of direct children and other descendants
        member.getChildren().forEach(child -> {
            child.setAncestry("/" + member.getId() + member.getAncestry());
            // then find all descendants whose ancestor is this child
            List<Member> furtherDescendants = repository.findAllByAncestryEndingWith(child.getId().toString() + "/");
            // appending new member's id to ancestry of further descendants (descendants startign with grandchildren)
            furtherDescendants.forEach(descendant -> descendant.setAncestry(descendant.getAncestry() + member.getId().toString() + "/"));
        });
    }

    @Override
    public MemberInfoResponse getMemberById(Long id) throws Exception {
        Optional<Member> optionalMember = repository.findById(id);
        if (optionalMember.isEmpty()) throw new Exception("Member does not exist in database.");
        return MemberInfoResponse.of(optionalMember.get().getInfo());
    }

    @Override
    public List<MemberBasicInfoResponse> getMembersInFamilyTree(Long familyTreeId) throws Exception {
        FamilyTree familyTree = familyTreeRepository.findById(familyTreeId).orElseThrow(() -> new Exception("Invalid family tree of member: tree does not exist."));
        List<Member> membersOfTree = repository.findAllByFamilyTree(familyTree);
        return membersOfTree.stream().map(MemberBasicInfoResponse::of).collect(Collectors.toList());
    }

    @Override
    public MemberInfoResponse updateMemberInfo(Long id, MemberInfoUpdateRequest req) throws Exception {
        Member memberToUpdate = repository.findById(id).orElseThrow(() -> new Exception("No member with id = " + id + " present in database."));
        memberToUpdate.setInfo(req.toEntity());
        return MemberInfoResponse.of(repository.save(memberToUpdate).getInfo());
    }

    @Transactional
    @Override
    public MemberBasicInfoResponse deleteMemberById(Long id, boolean removeMarriage) throws Exception {
        if (id == null || id < 1) throw new Exception("Passed invalid id.");
        Optional<Member> optionalMember = repository.findById(id);
        if (optionalMember.isEmpty()) throw new Exception("Member does not exist in database.");

        /* ------- STRUCTURAL VALIDATION ------- */
        //        todo: check if member is someones parent or child or spouse
        Member toBeDeleted = optionalMember.get();
        Optional<Marriage> marriage = marriageRepository.findMarriageByHusband_IdOrWife_Id(toBeDeleted.getId(), toBeDeleted.getId());
        if (marriage.isPresent() && marriage.get().isActive())
            if (removeMarriage) marriageRepository.deleteById(marriage.get().getId());
            else throw new Exception("Member is married, consider enabling marriage removal.");
        if (toBeDeleted.hasChildren() && toBeDeleted.hasParent())
            throw new Exception("Member has both parent and children - will cause splitting of family tree");
        if (!toBeDeleted.hasParent() && toBeDeleted.hasMoreThanOneChild())
            throw new Exception("Member has multiple children - will cause splitting of family tree");
        /* ------- STRUCTURAL VALIDATION ------- */

        /* ------- REMOVING REFERENCES ------- */
        if (toBeDeleted.hasChildren()) removeAncestorFromChildren(toBeDeleted);
        if (toBeDeleted.hasParent()) toBeDeleted.getParent().getChildren().remove(toBeDeleted);
        /* ------- REMOVING REFERENCES ------- */

        repository.deleteById(id);
        return MemberBasicInfoResponse.of(toBeDeleted);
    }

    private void removeAncestorFromChildren(Member toBeDeleted) {
//        remove reference that child has to this member
//        note that there can be only one child while deleting member, multiple children not allowed
        toBeDeleted.getChildren().stream().findFirst().ifPresent(child -> child.setParent(null));

//        toBeDeleted.getChildren().forEach(toBeDeleted::removeChild); // not possible, ConcurrentModificationException

//        getting all descendants for removing this member's id from the end
        List<Member> descendants = repository.findAllByAncestryEndingWith(toBeDeleted.getId() + "/");
        descendants.forEach(descendant -> {
            String[] split = descendant.getAncestry().split("/");
            String newAncestry = Arrays.stream(split).limit(split.length - 1).reduce("", (subAncestry, ancestorId) -> subAncestry + ancestorId + "/");
            descendant.setAncestry(newAncestry);
        });
    }

    @Override
    public KinshipResponse findKinshipBetweenMembers(Long firstMemberId, Long secondMemberId) throws Exception {
        Member member1 = repository.findById(firstMemberId).orElseThrow(() -> new Exception("Member with id = " + firstMemberId + " does not exist in database."));

        Member member2 = repository.findById(secondMemberId).orElseThrow(() -> new Exception("Member with id = " + secondMemberId + " does not exist in database."));

        /*-------------------------------- Next case is for situation where married persons were passed  ---------------------------------*/
        KinshipResponse result = new KinshipResponse().setMember1(member1.getInfo().getName()).setMember2(member2.getInfo().getName());

        marriageRepository.findMarriageByHusband_IdOrWife_Id(firstMemberId, secondMemberId).ifPresent((marriage) -> result.setDegree(0).setMember1Role("Muž").setMember2Role("Žena").setType(KinshipResponse.Type.LATERAL));
        marriageRepository.findMarriageByHusband_IdOrWife_Id(secondMemberId, firstMemberId).ifPresent((marriage) -> result.setDegree(0).setMember1Role("Žena").setMember2Role("Muž").setType(KinshipResponse.Type.LATERAL));
        /*---------------------------------------------------------------------------------------------------------------------------------*/

        // todo: allow different family trees
        if (!member1.getFamilyTree().equals(member2.getFamilyTree()))
            throw new Exception("Members cannot be kin, they are not from same family tree");

        return KinshipCalculator.calculate(member1, member2);
    }

    /**
     * @param familyTreeId Identifier of family tree whose members are going t be retrieved
     * @return {@link HierarchyResponse} is tree-like structure containing all members of family tree organised in hierarchy
     * @throws Exception If there is no family tree with given id | If family tree does not have root member
     */
    @Override
    public HierarchyResponse getMembersHierarchy(Long familyTreeId) throws Exception {
        FamilyTree familyTree = familyTreeRepository.findById(familyTreeId).orElseThrow(() -> new Exception("Invalid family tree of member: tree does not exist."));
        List<Member> membersOfTree = repository.findAllByFamilyTree(familyTree);

        List<HierarchyResponse> nodes = membersOfTree.stream().map(member -> new HierarchyResponse(member.getInfo().getName() + " " + member.getInfo().getSurname(), MemberBasicInfoResponse.of(member), new ArrayList<>())).collect(Collectors.toList());

//        firstly, we find root node of this family tree, that is node with ancestry = /
        HierarchyResponse root = nodes.stream().filter(node -> node.getBasicInfo().getAncestry().equals("/")).findFirst().orElse(null);
        if (root == null) return null;
//        now we check each node's parent, and if parent exist we add to it current node
        nodes.forEach(node -> {
            if (node.getBasicInfo().getAncestry().equals("/")) return;
            final String[] ancestors = node.getBasicInfo().getAncestry().split("/");
            final String parent = ancestors[1]; // because split works that way that first element is blank, therefore we take second element of array
            final int parentId = Integer.parseInt(parent);
            final Optional<HierarchyResponse> parentNode = nodes.stream().filter(n -> n.getBasicInfo().getId() == parentId).findFirst();
            parentNode.ifPresent(hierarchyResponse -> hierarchyResponse.addChild(node));
        });
        return root;
    }

}
