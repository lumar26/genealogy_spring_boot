package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.FamilyTreeInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;

import java.util.List;

/**
 * Spring boot RESTful service class that contains business logic for family trees.
 * This includes CRUD operations and some other domain-specific operations with family trees
 *
 * @author lumar26
 * @version 1.0
 */
public interface FamilyTreeService {
    /**
     * Service method that saves new family tree to database.
     * New family tree must not be null and must contain family tree title, otherwise error occurs
     * Owner user of family tree must be defined (must exist in database) in order for tre to be successfully saved
     *
     * @param familyTree family tree object to be saved
     * @return saved family tree
     * @throws IllegalStateException if user that is owner does not exist in database
     */
    FamilyTreeInfoResponse saveFamilyTree(FamilyTreeSaveUpdateRequest familyTree) throws Exception;

    /**
     * Service method that retrieves all family trees from database.
     *
     * @return list of family trees
     */
    List<FamilyTreeInfoResponse> getAllFamilyTrees();

    /**
     * Method that retrieves family tree based on passed id.
     * Passed id is expected to be valid (long value greater than 0)
     *
     * @param id unique identifier of family tree in database, find criteria, path variable
     * @return found family tree
     * @throws IllegalStateException if family tree does not exist in database
     */
    FamilyTreeInfoResponse getFamilyTreeById(Long id) throws Exception;

    /**
     * Method that deletes family tree based on passed id.
     * Id must be valid (long value greater than 0).
     *
     * @param id unique identifier of family tree in database, delete criteria, path variable
     * @return deleted family tree
     * @throws IllegalStateException if family tree does not exist in database, or if id is invalid
     */
    FamilyTreeInfoResponse deleteFamilyTreeById(Long id) throws Exception;

    /**
     * Method that updates family tree based on passed id with a new value, taking care of structural and value constraints.
     *
     * @param id unique identifier of family tree in database, delete criteria, path variable
     * @param familyTree new data for family tree to be saved
     * @return updated family tree
     * @throws IllegalStateException if family tree does not exist in database, if id is invalid or new data for family tree is invalid
     */
    FamilyTreeInfoResponse updateFamilyTree(Long id, FamilyTreeSaveUpdateRequest familyTree) throws Exception;

    /**
     * Method that retrieves all family trees for a given user.
     * User must not be null and must exist in database.
     *
     * @param owner user that owns family trees to be returned, search criteria
     * @return list of family trees
     * @throws IllegalStateException if user is null
     */
    List<FamilyTreeInfoResponse> getFamilyTreesByOwner(Long ownerId) throws Exception;
}
