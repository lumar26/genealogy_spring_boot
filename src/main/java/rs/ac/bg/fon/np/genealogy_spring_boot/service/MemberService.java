package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberInfoUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.HierarchyResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.KinshipResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberInfoResponse;

import java.util.List;

/**
 * Spring boot RESTful service class that contains business logic for members.
 * This includes CRUD operations and some other domain-specific operations with members.
 *
 * @author lumar26
 * @version 1.0
 */
public interface MemberService {
    /**
     * Method that saves new member to database.
     * ValidMemberDto must not be null, and owner and family tree to which member belongs must be defined.
     * If family tree is not empty (e.g. new member is not the first ember of tree) then member must be connected to some other member of tree, i.e. has to have defined parent or child that already exists in this family tree
     *
     * @param member new member to be saved
     * @return saved member
     * @throws IllegalStateException if new member is null, family tree or owner are not defined
     */
    MemberBasicInfoResponse saveMember(MemberSaveRequest member) throws Exception;

    /**
     * Method for retrieving existing member from database based on id.
     *
     * @param id unique identifier for searching member in database
     * @return fond member from database
     * @throws IllegalStateException if member does not exist
     */
    MemberInfoResponse getMemberById(Long id) throws Exception;

    /**
     * Service method that retrieves all members of family tree that is passed as argument, from database.
     *
     * @return list of members
     * @throws IllegalStateException if family tree does not exist
     */
    List<MemberBasicInfoResponse> getMembersInFamilyTree(Long familyTreeId) throws Exception;

    /**
     * Method for updating info part of existing member object in database.
     * This method does not affect relationship that member has with other members, nor changes owner or family tree of member.
     *
     * @param id unique identifier for searching member in database
     * @param memberInfo new info data for member
     * @return member with updated info
     * @throws IllegalStateException if member does not exist
     */
    MemberInfoResponse updateMemberInfo(Long id, MemberInfoUpdateRequest memberInfo) throws Exception;

    /**
     * Method that deletes member from database based on given id.
     * If 'removeMarriage' is true then, if member was married, marriage of member will be deleted from database.
     * If 'removeMarriage' is true then, if member was married, exception will be thrown.
     *
     * @param id unique identifier in database, delete criteria
     * @param removeMarriage indicates whether marriage should be deleted, if member is married
     * @return deleted member
     * @throws IllegalStateException if removeMarriage == false and member is married
     */
    MemberBasicInfoResponse deleteMemberById(Long id, boolean removeMarriage) throws Exception;

    KinshipResponse findKinshipBetweenMembers(Long firstMemberId, Long secondMemberId) throws Exception;

    HierarchyResponse getMembersHierarchy(Long familyTreeId) throws Exception;
}
