package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

import java.util.Optional;

/**
 * Interface that extends JpaRepository which provides all methods for basic CRUD operations, specialized for working with User entity.
 *
 * @author lumar26
 * @version 1.0
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Method that selects user entity from database with given username and email.
     *
     * @param username User's username
     * @param email User's email
     * @return Optional containing User with given credentials
     */
    Optional<User> findUserByUsernameOrEmail(String username, String email);

    /**
     * Method that selects user from database that has given username
     *
     * @param username User's username
     * @return User entity
     */
    User getByUsername(String username);



}
