package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MarriageSaveRequest {
    private LocalDate startDate;
    private LocalDate endDate;
    private Long husbandId;
    private Long wifeId;

}
