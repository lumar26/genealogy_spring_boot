package rs.ac.bg.fon.np.genealogy_spring_boot.kinship;

import org.springframework.stereotype.Component;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.KinshipResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class KinshipCalculator {
    /**
     * Util method that calculates kinship between to members in family tree.
     * Prerequisite is that both members are from the same family tree.
     *
     * @param member1 first member of relationship
     * @param member2 second member of relationship
     * @return KinshipResponse containing all information about relationship between two members of family tree
     * @throws Exception if kinship could not be determined based on two provided members and their ancestry fields
     */
    public static KinshipResponse calculate(Member member1, Member member2) throws Exception {

        KinshipResponse res = new KinshipResponse()
                .setMember1(member1.getInfo().getName())
                .setMember2(member2.getInfo().getName());

        List<Long> ancestors1 = Arrays.stream(member1.getAncestry().split("/"))
                .filter(ancestorId -> !ancestorId.isEmpty() && !ancestorId.isBlank())
                .map(Long::parseLong)
                .collect(Collectors.toList());

        List<Long> ancestors2 = Arrays.stream(member2.getAncestry().split("/"))
                .filter(ancestorId -> !ancestorId.isEmpty() && !ancestorId.isBlank())
                .map(Long::parseLong)
                .collect(Collectors.toList());

//        next two cases occur if one of given members is ancestor to another

        if (ancestors1.contains(member2.getId())) // if member2 is ancestor
            return res.setDegree(Math.abs(ancestors1.size() - ancestors2.size()))
                    .setMember1Role("Потомак")
                    .setMember2Role(determineAncestorsRole(Math.abs(ancestors1.size() - ancestors2.size()), member2.getInfo().getGender()))
                    .setType(KinshipResponse.Type.VERTICAL);

        if (ancestors2.contains(member1.getId())) // if member1 is ancestor
            return res.setDegree(Math.abs(ancestors2.size() - ancestors1.size()))
                    .setMember2Role("Потомак")
                    .setMember1Role(determineAncestorsRole(Math.abs(ancestors2.size() - ancestors1.size()), member1.getInfo().getGender()))
                    .setType(KinshipResponse.Type.VERTICAL);


//        in other cases it is lateral kinship
        Long commonAncestor = -1L;
        for (Long l : ancestors1)
            if (ancestors2.contains(l)) {
                commonAncestor = l;
                break;
            }

        if (commonAncestor == -1L) // case when there are neither vertical nor lateral kinship
            throw new Exception("Could not calculate kinship between members: " + member1.getInfo().getName() + " and " + member2.getInfo().getName() + ".");

        return res.setType(KinshipResponse.Type.LATERAL)
                .setMember1Role(
                        determineCousinRole(commonAncestor, member1.getAncestry(),
                                member2.getAncestry(), member1.getInfo().getGender(),
                                member2.getInfo().getGender()))
                .setMember1Role(determineCousinRole(commonAncestor, member2.getAncestry(),
                        member2.getAncestry(), member2.getInfo().getGender(),
                        member2.getInfo().getGender()))
                .setDegree(Math.max(ancestors1.indexOf(commonAncestor), ancestors2.indexOf(commonAncestor)) + 1) // degree => 'koleno'
                .setSameLevelCousins(ancestors1.size() == ancestors2.size());
    }

    private static String determineCousinRole(Long commonAncestor,
                                              String member1Ancestry, String member2Ancestry,
                                              MemberInfo.Gender member1Gender, MemberInfo.Gender member2Gender) {
        return "Рођак";
    }

    private static String determineAncestorsRole(int degree, MemberInfo.Gender gender) {
        KinshipResponse.Ancestor[] ancestors = KinshipResponse.Ancestor.values();

        if (gender.equals(MemberInfo.Gender.MALE)) return ancestors[degree - 1].getMale();
        return ancestors[degree - 1].getFemale();
    }
}
