package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RefreshTokenRequest {
    private String refreshToken;
}
