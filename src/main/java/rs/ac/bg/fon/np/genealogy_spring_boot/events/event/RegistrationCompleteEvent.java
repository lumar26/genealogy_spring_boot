package rs.ac.bg.fon.np.genealogy_spring_boot.events.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

@Getter
@Setter
public class RegistrationCompleteEvent extends ApplicationEvent {
    private String contextPath = "/";
    private User user;

    public RegistrationCompleteEvent(User source) {
        super(source);
        this.user = source;
    }
}
