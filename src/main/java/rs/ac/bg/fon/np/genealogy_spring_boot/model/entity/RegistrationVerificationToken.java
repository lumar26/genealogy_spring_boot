package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "registration_verification_token")
@Data
@NoArgsConstructor
public class RegistrationVerificationToken {

    @Transient
    private static final int EXPIRATION = 30; // after how long token expires. TODO: should be read from config file

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String token;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column(name = "expiration_date_time")
    private LocalDateTime expirationDateTime;

    public RegistrationVerificationToken(String token, User user) {
        this.token = token;
        this.user = user;
        this.expirationDateTime = calculateExpirationDate(EXPIRATION);
    }

    public LocalDateTime calculateExpirationDate(int minutes){
        return LocalDateTime.now().plus(minutes, ChronoUnit.MINUTES);
    }
}
