package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Entity that represents marriage, a special type of relationship between members from separate family trees.
 * Members included in marriage should be of different gender.
 *
 * @author lumar26
 * @version 1.0
 */
@Entity
@Table(name = "marriage")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Marriage {

    /**
     * Unique numeric identifier of member in database
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private Long id;

    /**
     * Date when two members were married. Default value is date is when marriage was saved.
     */
    @EqualsAndHashCode.Include
    @Column
    private LocalDate startDate;
    /**
     * Date when marriage is ended. If marriage still lasts this date is null.
     */
    @Column
    private LocalDate endDate;

    /**
     * One of the members in marriage, specifically male member in relationship. Husband cannot be female.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "husband_id")
    private Member husband;

    /**
     * One of the members in marriage, specifically female member in relationship. Wife cannot be male.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "wife_id")
    private Member wife;

    public Marriage(LocalDate start, Member husband, Member wife) {
        this.startDate = start;
        this.husband = husband;
        this.wife = wife;
    }

    /**
     * Method to determine if two members are still married.
     *
     * @return boolean true if marriage still lasts. False if marriage is terminated.
     */
    @JsonIgnore
    public boolean isActive() {
        return this.endDate == null && startDate != null && !startDate.isAfter(LocalDate.now());
    }
}
