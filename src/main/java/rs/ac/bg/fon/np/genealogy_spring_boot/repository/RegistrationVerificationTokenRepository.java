package rs.ac.bg.fon.np.genealogy_spring_boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.RegistrationVerificationToken;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;

public interface RegistrationVerificationTokenRepository extends JpaRepository<RegistrationVerificationToken, Long> {
    RegistrationVerificationToken findByUser(User user);
    RegistrationVerificationToken findByToken(String token);
}
