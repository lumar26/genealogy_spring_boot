package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.FamilyTreeInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.FamilyTreeService;

import javax.validation.Valid;
import java.util.List;

/**
 * Spring boot RESTful controller class that exposes endpoints for manipulating family trees, including CRUD operations and others
 * This class has references to service layer classes that contain business logic for family trees.
 *
 * @author lumar26
 * @version 1.0
 */
@RestController
@RequestMapping("/api/family_tree")
@CrossOrigin(origins = "http://localhost:3000",
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.PUT, RequestMethod.DELETE})
public class FamilyTreeController {
    /**
     * Spring boot Service class for operating with family trees
     */
    private final FamilyTreeService service;

    @Autowired
    public FamilyTreeController(FamilyTreeService service) {
        this.service = service;
    }

    /**
     * Api Endpoint for saving new family tree into database
     *
     * @param saveRequestPayload request payload that contains info necessary for saving new family tree in system
     * @return basic data about saved tree if saved successfully, error message otherwise
     */
    @PostMapping()
    public ResponseEntity<?> saveFamilyTree(@RequestBody @Valid FamilyTreeSaveUpdateRequest saveRequestPayload) {
        try {
            FamilyTreeInfoResponse saved = service.saveFamilyTree(saveRequestPayload);
            return ResponseEntity.ok(saved);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not save family tree. " + e.getMessage());
        }
    }

    /**
     * Endpoint for retrieving all family trees that are currently stored in database
     *
     * @return list of all family trees in database
     */
    @GetMapping()
    public List<FamilyTreeInfoResponse> getAllFamilyTrees() {
        return service.getAllFamilyTrees();
    }

    /**
     * Endpoint for retrieving family tree based on passed id from client side
     *
     * @param id unique identifier of family tree in database, find criteria, path variable
     * @return dto representing family tree from database if exists, error message if error occurred
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getFamilyTreeById(@PathVariable("id") Long id) {
        try {
            FamilyTreeInfoResponse familyTreeDto = service.getFamilyTreeById(id);
            return ResponseEntity.ok(familyTreeDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not retrieve family tree with id = " + id + ". " + e.getMessage());
        }
    }

    /**
     * Endpoint for deleting family tree based on passed id from client side
     *
     * @param id unique identifier of family tree in database, delete criteria, path variable
     * @return id of family tree which is deleted, error message if deleting could not be done
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteFamilyTree(@PathVariable("id") Long id) {
        try {
            FamilyTreeInfoResponse deleted = service.deleteFamilyTreeById(id);
            return ResponseEntity.ok(deleted.getId()); // todo: maybe return another type of response
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not delete family tree with id = " + id + ". " + e.getMessage());
        }
    }

    /**
     * Endpoint for deleting family tree based on passed id from client side
     *
     * @param id  unique identifier of family tree in database, find criteria, path variable
     * @param dto new data for family tree, request payload
     * @return req of family tree from database with updated fields, error message if error occurred
     */
    @PutMapping("/{id}")
    public ResponseEntity<?> updateFamilyTree(@PathVariable("id") Long id,
                                              @RequestBody @Valid FamilyTreeSaveUpdateRequest dto) {
        try {
            FamilyTreeInfoResponse updated = service.updateFamilyTree(id, dto);
            return ResponseEntity.ok(updated);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not update family tree with id = " + id + ". " + e.getMessage());
        }
    }

    /**
     * Endpoint for retrieving all family trees whose owner is with id passed from client side
     *
     * @param userId unique identifier for user that is owner (creator) of family trees, find criteria
     * @return list of family trees if retrieving is successful, error message if not successful
     */
    @GetMapping("/user/{user}")
    public ResponseEntity<?> getAllFamilyTreesForUser(@PathVariable("user") Long userId) {
        try {
            List<FamilyTreeInfoResponse> trees = service.getFamilyTreesByOwner(userId);
            return ResponseEntity.ok(trees);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Could not retrieve family trees of user with id = " + userId + ". " + e.getMessage());
        }
    }
}
