package rs.ac.bg.fon.np.genealogy_spring_boot.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Entity class that represents users account in database.
 *
 *  @author lumar26
 *  @version 1.0
 */
@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User implements UserDetails {

    /**
     * Inner enum that represents user's role in the system.
     */
    public enum Role {
        ROLE_ADMIN, ROLE_USER, ROLE_PREMIUM_USER
    }

    /**
     * Unique identifier of User entity in database. Generated automatically.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private Long id;

    /**
     * First name of this user
     */
    private String name;

    /**
     * Last name of this user
     */
    private String surname;

    private Role role;

    /**
     * User's email. Must be unique and must meet regex requirements for email.
     */
    @Column(unique = true)
    @EqualsAndHashCode.Include
    @Email
    private String email;

    /**
     * User's username. Must be unique.
     */
    @Column(unique = true)
    @EqualsAndHashCode.Include
    private String username;

    private String password;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean enabled = false;

    /**
     * Set of family trees that this user has created. If user is deleted, family trees he has created are not removed.
     */
    @ToString.Exclude
    @JsonManagedReference(value = "user-familyTree")
    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private Set<FamilyTree> familyTrees;

    public User(String name, String surname, String email, String username, String password, Role role) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    /**
     * Method for getting all authorities user has based on his role.
     *
     * @return all authorities that user has.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(() -> this.role.name());
    }

    /**
     * Method for checking if user's account has expired
     *
     * @return boolean that tells if account has expired.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Method for checking if user's account is not locked
     *
     * @return boolean that tells if account is not locked. (true if not locked)
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Method for checking if user's account credentials have expired
     *
     * @return boolean that tells if account's credentials have expired.
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Method for checking if user's account is enabled
     *
     * @return boolean that tells if account is enabled.
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
