package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.FamilyTreeInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.FamilyTreeRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.FamilyTreeService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FamilyTreeServiceImpl implements FamilyTreeService {
    private final FamilyTreeRepository repository;
    private final UserRepository userRepository;

    @Autowired
    public FamilyTreeServiceImpl(FamilyTreeRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public FamilyTreeInfoResponse saveFamilyTree(FamilyTreeSaveUpdateRequest dto) throws Exception {
        if (dto == null) throw new Exception("Family tree dto for saving must not be null");
        Optional<User> optionalUser = userRepository.findById(dto.getUserId());
        if (optionalUser.isEmpty()) throw new Exception("Owner does not exist in database.");
        FamilyTree toBeSaved = new FamilyTree()
                .setTitle(dto.getTitle())
                .setDescription(dto.getDescription())
                .setUser(optionalUser.get());
        return FamilyTreeInfoResponse.of(repository.save(toBeSaved));
    }

    @Override
    public List<FamilyTreeInfoResponse> getAllFamilyTrees() {
        List<FamilyTree> trees = repository.findAll();
        return trees.stream().map(FamilyTreeInfoResponse::of).collect(Collectors.toList());
    }

    @Override
    public FamilyTreeInfoResponse getFamilyTreeById(Long id) throws Exception {
        if (id == null || id <= 0) throw new Exception("Could not find family tree: invalid id.");
        Optional<FamilyTree> dbFamilyTree = repository.findById(id);
        if (dbFamilyTree.isEmpty())
            throw new Exception("Family tree with id: " + id + " does not exist");
        return FamilyTreeInfoResponse.of(dbFamilyTree.get());
    }

    @Override
    public FamilyTreeInfoResponse deleteFamilyTreeById(Long id) throws Exception {
        if (id == null || id < 1) throw new Exception("Invalid family tree id.");
        Optional<FamilyTree> dbFamilyTree = repository.findById(id);
        if (dbFamilyTree.isEmpty())
            throw new Exception("Family tee does  not exist.");
//        dbFamilyTree.get().removeAllMembers();
        repository.deleteById(id);
        return FamilyTreeInfoResponse.of(dbFamilyTree.get());
    }

    @Override
    public FamilyTreeInfoResponse updateFamilyTree(Long id, FamilyTreeSaveUpdateRequest dto) throws Exception {
        if (id == null || id <= 0) throw new Exception("Invalid id for family tree.");
        if (dto == null) throw new Exception("Family tree to update not provided.");

        Optional<User> optionalUser = userRepository.findById(dto.getUserId());
        if (optionalUser.isEmpty()) throw new Exception("Owner does not exist in database.");

        Optional<FamilyTree> optionalFamilyTree = repository.findById(id);
        if (optionalFamilyTree.isEmpty())
            throw new Exception("Family tree with id = " + id + "does  not exist.");
        FamilyTree treeInDatabase = optionalFamilyTree.get();
        treeInDatabase.setTitle(dto.getTitle());
        treeInDatabase.setDescription(dto.getDescription());
        treeInDatabase.setUser(optionalUser.get());
        return FamilyTreeInfoResponse.of(repository.save(treeInDatabase));
    }

    @Override
    public List<FamilyTreeInfoResponse> getFamilyTreesByOwner(Long ownerId) throws Exception {
        User owner = userRepository.findById(ownerId).orElseThrow(() -> new Exception("Owner does not exist in database."));
        List<FamilyTree> trees = repository.findAllByUser(owner);
        return trees.stream().map(FamilyTreeInfoResponse::of).collect(Collectors.toList());
    }
}
