package rs.ac.bg.fon.np.genealogy_spring_boot.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = {MemberSaveDtoValidator.class, MemberInfoDtoValidator.class})
@Target(ElementType.TYPE)
@Retention(RUNTIME)
public @interface ValidMemberDto {
    String message() default "Invalid member dto for saving";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
