package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * DTO that represents payload that should be accepted on user register request.
 *
 * @author lumar26
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class UserRegisterRequest {
    @NotBlank(message = "Username must not be blank")
    @Length(min = 4, message = "User's username must be longer than 3 characters")
    private String username;
    @NotBlank(message = "Password must not be blank")
    @Length(min = 6, message = "User's password must be longer than 5 characters")
    private String password;
    @Email(message = "Invalid user's email")
    private String email;
    @NotBlank(message = "User's name must not be blank")
    private String name;
    @NotBlank(message = "User's surname must not be blank")
    private String surname;
}
