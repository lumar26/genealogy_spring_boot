package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MarriageSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MarriageServiceTest;

import java.time.LocalDate;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class MarriageServiceImplTest extends MarriageServiceTest {

    @BeforeEach
    void setUp() {
        husband = new Member();
        husband.setId(1L);
        husband.setInfo(new MemberInfo());
        husband.getInfo().setGender(MemberInfo.Gender.MALE);
        wife = new Member();
        wife.setId(2L);
        wife.setInfo(new MemberInfo());
        wife.getInfo().setGender(MemberInfo.Gender.FEMALE);
        marriage = new Marriage(LocalDate.now(), husband, wife);

        saveRequest = new MarriageSaveRequest()
                .setEndDate(marriage.getEndDate())
                .setHusbandId(marriage.getHusband().getId())
                .setWifeId(marriage.getWife().getId())
                .setStartDate(marriage.getEndDate());
    }

    @AfterEach
    void tearDown() {
    }
}