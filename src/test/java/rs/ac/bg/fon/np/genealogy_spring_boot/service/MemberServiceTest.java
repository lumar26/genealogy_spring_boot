package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberInfoUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberBasicInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MemberInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.FamilyTreeRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MemberRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.MemberServiceImpl;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

public abstract class MemberServiceTest {

    protected FamilyTree familyTree;
    protected User user;
    protected Member member;
    protected Member parent;
    protected Member withUpdatedInfo;
    protected MemberSaveRequest saveRequest;

    @Mock
    protected MemberRepository memberRepository;
    @Mock
    protected UserRepository userRepository;
    @Mock
    protected FamilyTreeRepository familyTreeRepository;

    @InjectMocks
    protected MemberServiceImpl memberServiceImpl;


    @Test
    void saveMember_firstInFamilyTree() throws Exception {
        Mockito.when(memberRepository.findAllByFamilyTree(member.getFamilyTree())).thenReturn(List.of());
        Mockito.when(memberRepository.save(member)).thenReturn(member);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.of(familyTree));

        MemberBasicInfoResponse res = memberServiceImpl.saveMember(saveRequest);
        assertEquals(res, MemberBasicInfoResponse.of(member));
    }

    @Test
    void saveMember_withParent() throws Exception {
        member.setId(1L);
        parent.setId(2L);
        member.setParent(parent);
        saveRequest.setParentId(parent.getId());
        Mockito.when(memberRepository.findById(parent.getId())).thenReturn(Optional.of(parent));
        Mockito.when(memberRepository.save(any(Member.class))).thenReturn(member);
        Mockito.when(memberRepository.findAllByFamilyTree(familyTree)).thenReturn(List.of(parent));
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.of(familyTree));

        MemberBasicInfoResponse saved = memberServiceImpl.saveMember(saveRequest);
        assertEquals(saved, MemberBasicInfoResponse.of(member));
    }

    @Test
    void saveMember_memberIsNull() {
        assertThrows(Exception.class, () -> memberServiceImpl.saveMember(null));
    }

    @Test
    void saveMember_ownerOfMemberIsNull() {
        member.setUser(null);
        assertThrows(Exception.class, () -> memberServiceImpl.saveMember(saveRequest));
    }

    @Test
    void saveMember_familyTreeIsNull() {
        member.setFamilyTree(null);
        assertThrows(Exception.class, () -> memberServiceImpl.saveMember(saveRequest));
    }

    @Test
    void saveMember_notRelatedToOtherMembersOfFamilyTree_treeNotEmpty() {
        Mockito.when(memberRepository.findAllByFamilyTree(familyTree)).thenReturn(List.of(parent));
        assertThrows(Exception.class, () -> memberServiceImpl.saveMember(saveRequest));
    }

    @Test
    void getMemberById() throws Exception {
        member.setId(1L);
        Mockito.when(memberRepository.findById(1L)).thenReturn(Optional.of(member));
        MemberInfoResponse found = memberServiceImpl.getMemberById(1L);
        assertEquals(found, MemberInfoResponse.of(member.getInfo()));
    }

    @Test
    void getMemberById_memberDoesNotExist() {
        member.setId(1L);
        Mockito.when(memberRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> memberServiceImpl.getMemberById(1L));
    }

    @Test
    void getMembersInFamilyTree() throws Exception {
        Mockito.when(memberRepository.findAllByFamilyTree(familyTree)).thenReturn(List.of(member, parent));
        Mockito.when(familyTreeRepository
                .findById(familyTree.getId())).thenReturn(Optional.of(familyTree));

        List<MemberBasicInfoResponse> membersOfTree = memberServiceImpl.getMembersInFamilyTree(familyTree.getId());
        assertEquals(membersOfTree, List.of(MemberBasicInfoResponse.of(member), MemberBasicInfoResponse.of(parent)));
    }

    @Test
    void getMembersInFamilyTree_familyTreeIsNull() {
        assertThrows(Exception.class, () -> memberServiceImpl.getMembersInFamilyTree(null));
    }

    @Test
    void updateMember() throws Exception {
        member.setId(1L);
        withUpdatedInfo.setId(1L);
        Mockito.when(memberRepository.save(withUpdatedInfo)).thenReturn(withUpdatedInfo);
        Mockito.when(memberRepository.findById(member.getId())).thenReturn(Optional.of(member));

        MemberInfoUpdateRequest req = new MemberInfoUpdateRequest()
                .setName(withUpdatedInfo.getInfo().getName())
                .setSurname(withUpdatedInfo.getInfo().getSurname())
                .setBirthDate(withUpdatedInfo.getInfo().getBirthDate())
                .setBirthPlace(withUpdatedInfo.getInfo().getBirthPlace())
                .setDeathDate(withUpdatedInfo.getInfo().getDeathDate())
                .setDeathPlace(withUpdatedInfo.getInfo().getDeathPlace())
                .setGender(withUpdatedInfo.getInfo().getGender().name())
                .setCountry(withUpdatedInfo.getInfo().getCurrentAddress().getCountry())
                .setCityOrMunicipality(withUpdatedInfo.getInfo().getCurrentAddress().getCityOrMunicipality())
                .setPostalCode(withUpdatedInfo.getInfo().getCurrentAddress().getPostalCode())
                .setStreet(withUpdatedInfo.getInfo().getCurrentAddress().getStreet())
                .setNumber(withUpdatedInfo.getInfo().getCurrentAddress().getNumber());

        MemberInfoResponse updated = memberServiceImpl.updateMemberInfo(member.getId(), req);
        assertEquals(updated, MemberInfoResponse.of(withUpdatedInfo.getInfo()));
    }
}