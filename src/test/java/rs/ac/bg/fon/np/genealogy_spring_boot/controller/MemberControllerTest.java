package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import rs.ac.bg.fon.np.genealogy_spring_boot.Config;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MemberService;

import java.time.LocalDate;
import java.time.Month;

@WebMvcTest(MemberController.class)
@Import(Config.class)
class MemberControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    MemberService service;

    ObjectMapper om = new ObjectMapper().registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    private User user;
    private FamilyTree familyTree;
    private Member member;

    @BeforeEach
    void setup() {
        user = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        user.setId(1L);
        familyTree = new FamilyTree("Marinkovici", "Marinkovici iz krezbinac", user);
        familyTree.setId(1L);
        Address address = new Address("Srbija", "35250", "Krezbinac", "Djure Jaksica", "3");
        MemberInfo info = new MemberInfo("Dejan", "Marinkovic", MemberInfo.Gender.MALE,
                LocalDate.of(1967, Month.AUGUST, 30), null, "Paracin", null, address);
        member = new Member(user, familyTree, info, null, null);

        this.mockMvc = MockMvcBuilders.standaloneSetup(new MemberController(service)).build();
    }

    @AfterEach
    void tearDown() {
        user = null;
        familyTree = null;
        member = null;
    }

    @Test
    void saveMember_ok() throws Exception {
//        Mockito.when(userService.getUserById(user.getId())).thenReturn(user);
//        Mockito.when(familyTreeService.getFamilyTreeById(familyTree.getId())).thenReturn(familyTree);
//        Mockito.when(service.saveMember(member)).thenReturn(member);
//
//        String payload = "{\n" +
//                "  \"name\": \"Dejan\",\n" +
//                "  \"surname\": \"Marinkovic\",\n" +
//                "  \"gender\": \"MALE\",\n" +
//                "  \"birthPlace\": \"Paracin\",\n" +
//                "  \"birthDate\": \"1967-08-30\",\n" +
//                "  \"country\": \"Srbija\",\n" +
//                "  \"street\": \"Djure Jaksica\",\n" +
//                "  \"number\": \"3\",\n" +
//                "  \"cityOrMunicipality\": \"Krezbinac\",\n" +
//                "  \"postalCode\": \"35250\",\n" +
//                "  \"userId\": 1,\n" +
//                "  \"familyTreeId\": 1\n" +
//                "}";
//
//
//        mockMvc.perform(
//                post("/api/member")
//                        .content(payload)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .characterEncoding(StandardCharsets.UTF_8)
//                        .accept(MediaType.APPLICATION_JSON)
//        ).andExpect(status().isOk());
//
//        MvcResult result = mockMvc.perform(
//                        post("/api/member")
//                                .content(payload)
//                                .contentType(MediaType.APPLICATION_JSON)
//                                .characterEncoding(StandardCharsets.UTF_8)
//                                .accept(MediaType.APPLICATION_JSON)
//                )
//                .andDo(print())
//                .andExpectAll(
//                        status().isOk()
//                )
//                .andReturn();
//        String resultContent = result.getResponse().getContentAsString();
//        System.out.println(" ==== saved member result content = " + resultContent);
//        ValidMemberDto saved = om.readValue(resultContent, ValidMemberDto.class);
//
//        Assertions.assertEquals(saved, member);
    }

    @Test
    void getMemberById() throws Exception {
//        member.setId(1L);
//        Mockito.when(service.getMemberById(member.getId())).thenReturn(member);
//        MvcResult result = mockMvc.perform(get("/api/member/{id}", member.getId()))
//                .andDo(print())
//                .andExpect(status().isOk()).andReturn();
//
//        String resultContent = result.getResponse().getContentAsString();
//        Member retrieved = om.readValue(resultContent, Member.class);
//        Assertions.assertEquals(retrieved, member);
    }

    @Test
    void getMembersOfFamilyTree_ok() throws Exception {
//        Address address = new Address("Srbija", "35250", "Krezbinac", "Djure Jaksica", "3");
//        MemberInfo info = new MemberInfo("Luka", "Marinkovic", MemberInfo.Gender.MALE,
//                LocalDate.of(2000, Month.FEBRUARY, 26), null, "Paracin", null, address);
//        Member anotherMember = new Member(user, familyTree, info, null, null);
//
//        List<Member> membersInTree = List.of(member, anotherMember);
//        Mockito.when(familyTreeService.getFamilyTreeById(familyTree.getId())).thenReturn(familyTree);
//        Mockito.when(service.getMembersInFamilyTree(familyTree)).thenReturn(membersInTree);
//
//        MvcResult result = mockMvc.perform(get("/api/member/family_tree/{familyTree}", familyTree.getId()))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//        String resultContent = result.getResponse().getContentAsString();
//        Member[] retrieved = om.readValue(resultContent, Member[].class);
//        Assertions.assertArrayEquals(retrieved, new Member[]{member, anotherMember});
    }

    @Test
    void updateMemberInfo() throws Exception {
//        member.setId(1L);
//        Address newAddress = new Address("Srbija", "35250", "Paracin", "Djure Jaksica", "3");
//        MemberInfo newInfo = new MemberInfo("Deja", "Marinkovicc", MemberInfo.Gender.FEMALE,
//                LocalDate.of(1967, Month.AUGUST, 30), null, "Paracin", null, newAddress);
//        Member updatedMember = new Member(user, familyTree, newInfo, null, null);
//        updatedMember.setId(member.getId());
//
//        Mockito.when(service.updateMemberInfo(member.getId(), newInfo))
//                .thenReturn(updatedMember);
//
//        MvcResult result = mockMvc.perform(
//                        put("/api/member/info/{id}", member.getId())
//                                .content(om.writeValueAsString(newInfo))
//                                .contentType(MediaType.APPLICATION_JSON_VALUE)
//                                .characterEncoding(StandardCharsets.UTF_8)
//                                .accept(MediaType.APPLICATION_JSON)
//                )
//                .andDo(print())
//                .andExpectAll(
//                        status().isOk()
//                )
//                .andReturn();
//        String resultContent = result.getResponse().getContentAsString();
//        MemberInfoResponse updatedDto = om.readValue(resultContent, MemberInfoResponse.class);
//
//        Assertions.assertEquals(updatedDto, MemberInfoResponse.of(newInfo));
    }

    @Test
    void deleteMemberById() throws Exception {
//        member.setId(1L);
//        Mockito.when(service.deleteMemberById(member.getId(), true)).thenReturn(member);
//        MvcResult result = mockMvc.perform(delete("/api/member/{id}", member.getId()))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//        String resultContent = result.getResponse().getContentAsString();
//        MemberBasicInfoResponse deleted = om.readValue(resultContent, MemberBasicInfoResponse.class);
//        Assertions.assertEquals(deleted, new MemberBasicInfoResponse(member));
    }
}