package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.FamilyTreeServiceTest;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class FamilyTreeServiceImplTest extends FamilyTreeServiceTest {
    @BeforeEach
    private void setUp() {
        user = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        familyTree = new FamilyTree("FamilyTree1", "FT1", user);
        inDto = new FamilyTreeSaveUpdateRequest().setTitle(familyTree.getTitle())
                .setDescription(familyTree.getDescription())
                .setUserId(familyTree.getUser().getId());
    }
}