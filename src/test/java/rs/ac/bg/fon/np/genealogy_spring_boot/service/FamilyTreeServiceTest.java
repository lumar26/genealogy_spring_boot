package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.FamilyTreeInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.FamilyTreeRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.FamilyTreeServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public abstract class FamilyTreeServiceTest {

    @Mock
    protected FamilyTreeRepository familyTreeRepository;
    @Mock
    protected UserRepository userRepository;

    @InjectMocks
    protected FamilyTreeServiceImpl service;

    protected User user;
    protected FamilyTree familyTree;
    protected FamilyTreeSaveUpdateRequest inDto;

    @Test
    void saveFamilyTree() throws Exception {
        Mockito.when(familyTreeRepository.save(familyTree)).thenReturn(familyTree);
        Mockito.when(userRepository.findById(familyTree.getUser().getId())).thenReturn(Optional.of(familyTree.getUser()));

        FamilyTreeInfoResponse outDto = new FamilyTreeInfoResponse()
                .setId(familyTree.getId())
                .setTitle(familyTree.getTitle())
                .setDescription(familyTree.getDescription())
                .setOwner(familyTree.getUser().getUsername());

        FamilyTreeInfoResponse saved = service.saveFamilyTree(inDto);
        assertEquals(saved, outDto);
    }

    @Test
    void saveFamilyTree_userDoesNotExist() {
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.empty());
        FamilyTreeSaveUpdateRequest inDto = new FamilyTreeSaveUpdateRequest().setTitle(familyTree.getTitle())
                .setDescription(familyTree.getDescription())
                .setUserId(familyTree.getUser().getId());
        assertThrows(Exception.class, () -> service.saveFamilyTree(inDto));
    }

    @Test
    void saveFamilyTree_treeIsNull() {
        assertThrows(Exception.class, () -> service.saveFamilyTree(null));
    }

    @Test
    void getAllFamilyTrees() {
        List<FamilyTree> familyTreesInDatabase = List.of(
                familyTree,
                new FamilyTree("FamilyTree2", "FamilyTree2", user),
                new FamilyTree("FamilyTree3", "FamilyTree3", user)
        );

        Mockito.when(familyTreeRepository.findAll()).thenReturn(familyTreesInDatabase);

        List<FamilyTreeInfoResponse> retrieved = service.getAllFamilyTrees();
        assertEquals(retrieved, familyTreesInDatabase.stream().map(FamilyTreeInfoResponse::of).collect(Collectors.toList()));
    }

    @Test
    void getFamilyTreeById() throws Exception {
        familyTree.setId(1L);
        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.of(familyTree));
        FamilyTreeInfoResponse found = service.getFamilyTreeById(1L);
        assertEquals(FamilyTreeInfoResponse.of(familyTree), found);
    }

    @Test
    void getFamilyTreeById_treeDoesNotExist() {
        familyTree.setId(1L);
        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> service.getFamilyTreeById(familyTree.getId()));
    }

    @Test
    void getFamilyTreeById_invalidId() {
        assertThrows(Exception.class, () -> service.getFamilyTreeById(null));
    }

    @Test
    void deleteFamilyTreeById() throws Exception {
        familyTree.setId(69L);
        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.of(familyTree));
        Mockito.doNothing().when(familyTreeRepository).deleteById(familyTree.getId());

        assertEquals(FamilyTreeInfoResponse.of(familyTree), service.deleteFamilyTreeById(familyTree.getId()));
    }

    @Test
    void deleteFamilyTreeById_invalidId() {
        assertThrows(Exception.class, () -> service.deleteFamilyTreeById(null));
        assertThrows(Exception.class, () -> service.deleteFamilyTreeById(-1L));
        assertThrows(Exception.class, () -> service.deleteFamilyTreeById(0L));
    }

    @Test
    void deleteFamilyTreeById_treeDoesNotExist() {
        familyTree.setId(69L);
        assertThrows(Exception.class, () -> service.deleteFamilyTreeById(1L));
    }

    @Test
    void updateFamilyTree() throws Exception {
        familyTree.setId(1L);
        FamilyTree updatedFamilyTree = new FamilyTree("FamilyTree2", "FT2", user);
        updatedFamilyTree.setId(familyTree.getId());

        inDto = new FamilyTreeSaveUpdateRequest().setTitle(updatedFamilyTree.getTitle())
                .setDescription(updatedFamilyTree.getDescription())
                .setUserId(updatedFamilyTree.getUser().getId());

        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.of(familyTree));
        Mockito.when(familyTreeRepository.save(updatedFamilyTree)).thenReturn(updatedFamilyTree);
        Mockito.when(userRepository.findById(familyTree.getUser().getId())).thenReturn(Optional.of(familyTree.getUser()));

        FamilyTreeInfoResponse updated = service.updateFamilyTree(familyTree.getId(), inDto);
        updatedFamilyTree.setId(familyTree.getId());
        assertEquals(updated, FamilyTreeInfoResponse.of(updatedFamilyTree));
    }

    @Test
    void updateFamilyTree_invalidId() {
        assertThrows(Exception.class, () -> service.updateFamilyTree(null, inDto));
        assertThrows(Exception.class, () -> service.updateFamilyTree(0L, inDto));
        assertThrows(Exception.class, () -> service.updateFamilyTree(-1L, inDto));
    }

    @Test
    void updateFamilyTree_invalidNewValue() {
        assertThrows(Exception.class, () -> service.updateFamilyTree(1L, null));
    }

    @Test
    void updateFamilyTree_familyTreeDoesNotExist() {
        familyTree.setId(1L);

        Mockito.when(familyTreeRepository.findById(familyTree.getId())).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> service.updateFamilyTree(familyTree.getId(), inDto));
    }

    @Test
    void getFamilyTreesByOwner() throws Exception {
        FamilyTree existent1 = new FamilyTree("FamilyTree1", "FT1", user);
        FamilyTree existent2 = new FamilyTree("FamilyTree2", "FT2", user);
        FamilyTree existent3 = new FamilyTree("FamilyTree3", "FT3", user);
        existent1.setId(1L);
        existent2.setId(2L);
        existent3.setId(3L);

        List<FamilyTree> treesInDatabase = List.of(existent1, existent2, existent3);

        Mockito.when(familyTreeRepository.findAllByUser(user)).thenReturn(treesInDatabase);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        List<FamilyTreeInfoResponse> found = service.getFamilyTreesByOwner(user.getId());
        assertEquals(treesInDatabase.stream().map(FamilyTreeInfoResponse::of).collect(Collectors.toList()), found);
    }

    @Test
    void getFamilyTreesByOwner_invalidOwner() {
        assertThrows(Exception.class, () -> service.getFamilyTreesByOwner(null));
    }
}