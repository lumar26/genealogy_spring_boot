package rs.ac.bg.fon.np.genealogy_spring_boot.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MarriageSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.MarriageInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MarriageRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.MemberRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.MarriageServiceImpl;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public abstract class MarriageServiceTest {

    protected Marriage marriage;
    protected Member husband;
    protected Member wife;
    protected MarriageSaveRequest saveRequest;

    @Mock
    protected MarriageRepository repository;
    @Mock
    protected MemberRepository memberRepository;

    @InjectMocks
    protected MarriageServiceImpl service;

    @Test
    void saveMarriage() throws Exception {
        Mockito.when(repository.findAllByHusbandEqualsOrWifeEquals(husband, wife))
                .thenReturn(List.of());
        Mockito.when(repository.save(marriage)).thenReturn(marriage);
        Mockito.when(memberRepository.findById(husband.getId())).thenReturn(Optional.of(husband));
        Mockito.when(memberRepository.findById(wife.getId())).thenReturn(Optional.of(wife));


        Assertions.assertEquals(MarriageInfoResponse.of(marriage), service.saveMarriage(saveRequest));
    }

    @Test
    void saveMarriage_spouseAlreadyMarried() {
        Mockito.when(repository.findAllByHusbandEqualsOrWifeEquals(husband, wife))
                .thenReturn(List.of(marriage));
        Mockito.when(repository.save(marriage)).thenReturn(marriage);
        Mockito.when(memberRepository.findById(husband.getId())).thenReturn(Optional.of(husband));
        Mockito.when(memberRepository.findById(wife.getId())).thenReturn(Optional.of(wife));

        Assertions.assertThrows(IllegalStateException.class, () -> service.saveMarriage(saveRequest));
    }

    @Test
    void saveMarriage_spousesAreSameGender() {
        husband.getInfo().setGender(MemberInfo.Gender.FEMALE);
        Mockito.when(repository.findAllByHusbandEqualsOrWifeEquals(husband, wife))
                .thenReturn(List.of(marriage));
        Assertions.assertThrows(Exception.class, () -> service.saveMarriage(saveRequest));

        husband.getInfo().setGender(MemberInfo.Gender.MALE);
        wife.getInfo().setGender(MemberInfo.Gender.MALE);
        Assertions.assertThrows(Exception.class, () -> service.saveMarriage(saveRequest));
    }

    @Test
    void saveMarriage_invertedGenders() throws Exception {
        husband.getInfo().setGender(MemberInfo.Gender.FEMALE);
        wife.getInfo().setGender(MemberInfo.Gender.MALE);
        Mockito.when(repository.findAllByHusbandEqualsOrWifeEquals(husband, wife))
                .thenReturn(List.of());

//        should be normal after save
        husband.getInfo().setGender(MemberInfo.Gender.MALE);
        wife.getInfo().setGender(MemberInfo.Gender.FEMALE);

        Mockito.when(memberRepository.findById(husband.getId())).thenReturn(Optional.of(husband));
        Mockito.when(memberRepository.findById(wife.getId())).thenReturn(Optional.of(wife));

        Mockito.when(repository.save(marriage)).thenReturn(marriage);
        Assertions.assertEquals(MarriageInfoResponse.of(marriage), service.saveMarriage(saveRequest));
    }

    @Test
    void getMarriageOfMember() throws Exception {
        Mockito.when(repository.findMarriageByHusband_IdOrWife_Id(husband.getId(), husband.getId()))
                .thenReturn(Optional.of(marriage));
        Assertions.assertEquals(MarriageInfoResponse.of(marriage), service.getMarriageOfMember(husband.getId()));
    }

    @Test
    void getMarriageOfMember_invalidMember() {
        Assertions.assertThrows(IllegalStateException.class, () -> service.getMarriageOfMember(null));
        husband.setId(null);
        Assertions.assertThrows(IllegalStateException.class, () -> service.getMarriageOfMember(husband.getId()));
    }

    @Test
    void getMarriageOfMember_memberIsNotMarried() {
        Mockito.when(repository.findMarriageByHusband_IdOrWife_Id(husband.getId(), husband.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(Exception.class, () -> service.getMarriageOfMember(husband.getId()));
    }

    @Test
    void deleteMarriageById() {
        marriage.setId(1L);
        Mockito.when(repository.findById(marriage.getId())).thenReturn(Optional.of(marriage));
        Mockito.doNothing().when(repository).delete(marriage);
        Assertions.assertEquals(MarriageInfoResponse.of(marriage), service.deleteMarriageById(marriage.getId()));
    }

    @Test
    void deleteMarriageById_marriageDoesNotExist() {
        marriage.setId(1L);
        Mockito.when(repository.findById(marriage.getId())).thenReturn(Optional.empty());
        Mockito.doNothing().when(repository).delete(marriage);
        Assertions.assertThrows(IllegalStateException.class, () -> service.deleteMarriageById(marriage.getId()));
    }

    @Test
    void deleteMarriageById_invalidId() {
        Assertions.assertThrows(IllegalStateException.class, () -> service.deleteMarriageById(0L));
        Assertions.assertThrows(IllegalStateException.class, () -> service.deleteMarriageById(-1L));
    }
}