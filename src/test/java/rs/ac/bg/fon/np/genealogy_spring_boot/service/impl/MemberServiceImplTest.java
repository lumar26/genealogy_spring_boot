package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.MemberSaveRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.*;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MemberServiceTest;

import java.time.LocalDate;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class MemberServiceImplTest extends MemberServiceTest {
    @BeforeEach
    void setUp() {
        user = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        user.setId(1L);

        familyTree = new FamilyTree("Marinkovici", "Marinkovici iz krezbinac", user);
        familyTree.setId(1L);

        Address address = new Address("Srbija", "35250", "Paracin", "Djure Jaksica", "3");

        Address addressOfUpdated = new Address("Srbija", "11000", "Beograd, Vozdovac", "Niksicka", "6/B");

        MemberInfo info = new MemberInfo("Dejan", "Marinkovic", MemberInfo.Gender.MALE, LocalDate.of(1967, 8, 30), null, "Paracin", null, address);
        member = new Member(user, familyTree, info, null, null);

        MemberInfo parentInfo = new MemberInfo("Ratomir", "Marinkovic", MemberInfo.Gender.MALE, LocalDate.of(1942, 5, 5), null, "Krezbinac", null, address);
        parent = new Member(user, familyTree, parentInfo, null, null);

        MemberInfo updatedInfo = new MemberInfo("Deja", "Marinkovic", MemberInfo.Gender.MALE, LocalDate.of(1967, 8, 30), null, "Krezbinac", null, addressOfUpdated);
        withUpdatedInfo = new Member(user, familyTree, updatedInfo, null, null);

        saveRequest = new MemberSaveRequest(user.getId(), familyTree.getId(), null, null,
                member.getInfo().getName(), member.getInfo().getGender().name(), member.getInfo().getSurname(),
                member.getInfo().getBirthDate(), member.getInfo().getDeathDate(), member.getInfo().getDeathPlace(), member.getInfo().getBirthPlace(),
                member.getInfo().getCurrentAddress().getCountry(), member.getInfo().getCurrentAddress().getPostalCode(), member.getInfo().getCurrentAddress().getCityOrMunicipality(),
                member.getInfo().getCurrentAddress().getStreet(), member.getInfo().getCurrentAddress().getNumber());

    }
}