package rs.ac.bg.fon.np.genealogy_spring_boot.model.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MemberSaveRequestTest {
    @Autowired
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testMemberSaveDto_To_Member(){

    }
}