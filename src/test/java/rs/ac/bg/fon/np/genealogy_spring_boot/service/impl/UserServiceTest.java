package rs.ac.bg.fon.np.genealogy_spring_boot.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.repository.UserRepository;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.UserRegisterRequest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class UserServiceTest {


    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;


    @Test
    void registerUser() throws Exception {
        User userToRegister = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        User userInDatabase = new User("Lazar", "Marinkovic", "laza@gmail.com", "laza", "laza", User.Role.ROLE_USER);
        UserRegisterRequest dto = new UserRegisterRequest("luka", "luka", "luka@gmail.com", "Luka", "Marinkovic");

        Mockito.when(userRepository.findUserByUsernameOrEmail(userInDatabase.getUsername(), userInDatabase.getEmail()))
                .thenReturn(Optional.of(userInDatabase));
        Mockito.when(passwordEncoder.encode(dto.getPassword())).thenReturn(dto.getPassword());
        Mockito.when(userRepository.save(userToRegister)).thenReturn(userToRegister);

        User registered = userService.registerUser(dto);
        assertEquals(registered, userToRegister);
    }

    @Test
    void registerUser_userAlreadyExists() {
        User userInDatabase = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        UserRegisterRequest dto = new UserRegisterRequest("luka", "luka", "luka@gmail.com", "Luka", "Marinkovic");

        Mockito.when(userRepository.findUserByUsernameOrEmail(userInDatabase.getUsername(), userInDatabase.getEmail())).thenReturn(Optional.of(userInDatabase));
        Mockito.when(userRepository.save(userInDatabase)).thenReturn(userInDatabase);

        assertThrows(Exception.class, () -> userService.registerUser(dto));
    }

    @Test
    void registerUser_passedUserIsNull() {
        assertThrows(IllegalStateException.class, () -> userService.registerUser(null));
    }

    @Test
    void getUserById() throws Exception {
        User u1 = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        u1.setId(1L);

        Mockito.when(userRepository.findById(u1.getId())).thenReturn(Optional.of(u1));

        User found = userService.getUserById(u1.getId());
        System.out.println(found);
        assertEquals(u1, found);
    }

    @Test
    void getUserById_invalidId() {
        assertThrows(Exception.class, () -> userService.getUserById(null));
        assertThrows(Exception.class, () -> userService.getUserById(0L));
        assertThrows(Exception.class, () -> userService.getUserById(-1L));
    }

    @Test
    void getUserById_userDoesNotExist() {
        User u1 = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        u1.setId(1L);

        Mockito.when(userRepository.findById(u1.getId())).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> userService.getUserById(u1.getId()));
    }
}