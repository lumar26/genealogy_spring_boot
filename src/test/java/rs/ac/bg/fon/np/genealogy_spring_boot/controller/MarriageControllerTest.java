package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import rs.ac.bg.fon.np.genealogy_spring_boot.Config;
import rs.ac.bg.fon.np.genealogy_spring_boot.controller.MarriageController;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Member;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.MemberInfo;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MemberService;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.Marriage;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.MarriageService;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.impl.UserService;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MarriageController.class)
@Import(Config.class)
class MarriageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MemberService memberService;
    @MockBean
    private MarriageService marriageService;
    @MockBean
    UserService userService;

    private Marriage marriage;
    private Member husband;
    private Member wife;

    @BeforeEach
    void setUp() {
        husband = new Member();
        husband.setId(1L);
        husband.setInfo(new MemberInfo());
        husband.getInfo().setGender(MemberInfo.Gender.MALE);
        wife = new Member();
        wife.setId(2L);
        wife.setInfo(new MemberInfo());
        wife.getInfo().setGender(MemberInfo.Gender.FEMALE);
        marriage = new Marriage(null, husband, wife);
    }

    @Test
    @WithMockUser
    void addMarriage() throws Exception {
//        Mockito.when(memberService.getMemberById(husband.getId())).thenReturn(husband);
//        Mockito.when(memberService.getMemberById(wife.getId())).thenReturn(wife);
//        Mockito.when(marriageService.saveMarriage(marriage)).thenReturn(marriage);
//
//        MvcResult result = mockMvc.perform(post("/api/marriage")
//                        .content(new ObjectMapper().writeValueAsString(marriage))
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .characterEncoding(StandardCharsets.UTF_8)
//                        .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//        String resultContent = result.getResponse().getContentAsString();
//        Marriage saved = new ObjectMapper().readValue(resultContent, Marriage.class);
//        Assertions.assertEquals(saved, marriage);
    }
}