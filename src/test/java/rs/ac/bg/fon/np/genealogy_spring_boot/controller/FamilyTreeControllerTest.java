package rs.ac.bg.fon.np.genealogy_spring_boot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import rs.ac.bg.fon.np.genealogy_spring_boot.Config;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.request.FamilyTreeSaveUpdateRequest;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.dto.response.FamilyTreeInfoResponse;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.FamilyTree;
import rs.ac.bg.fon.np.genealogy_spring_boot.model.entity.User;
import rs.ac.bg.fon.np.genealogy_spring_boot.service.FamilyTreeService;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FamilyTreeController.class)
@Import(Config.class)
class FamilyTreeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    FamilyTreeService service;

    ObjectMapper om = new ObjectMapper();

    private User user;
    private FamilyTree familyTree;

    @BeforeEach
    void setUp() {
        user = new User("Luka", "Marinkovic", "luka@gmail.com", "luka", "luka", User.Role.ROLE_USER);
        user.setId(1L);
        familyTree = new FamilyTree("Marinkovici iz Krezbinca", "Rodoslov familije Marinkovic iz Krezbinca", user);

        this.mockMvc = MockMvcBuilders.standaloneSetup(new FamilyTreeController(service)).build();
    }

    @Test
    void saveFamilyTree_ok() throws Exception {
        FamilyTreeSaveUpdateRequest reqDto = new FamilyTreeSaveUpdateRequest("Marinkovici iz Krezbinca",
                "Rodoslov familije Marinkovic iz Krezbinca", user.getId());

        FamilyTreeInfoResponse outDto = new FamilyTreeInfoResponse()
                .setId(familyTree.getId())
                .setTitle(familyTree.getTitle())
                .setDescription(familyTree.getDescription())
                .setOwner(familyTree.getUser().getUsername());

        Mockito.when(service.saveFamilyTree(reqDto)).thenReturn(outDto);

        MvcResult result = mockMvc.perform(
                        post("/api/family_tree")
                                .content(om.writeValueAsString(reqDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .characterEncoding(StandardCharsets.UTF_8)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String resultContent = result.getResponse().getContentAsString();
        FamilyTreeInfoResponse saved = om.readValue(resultContent, FamilyTreeInfoResponse.class);
        Assertions.assertEquals(saved, FamilyTreeInfoResponse.of(familyTree));
    }

    @Test
    void getFamilyTreeById_ok() throws Exception {
        familyTree.setId(1L);
        Mockito.when(service.getFamilyTreeById(familyTree.getId())).thenReturn(FamilyTreeInfoResponse.of(familyTree));
        MvcResult result = mockMvc.perform(get("/api/family_tree/{id}", familyTree.getId()))
                .andExpectAll(
                        status().isOk()
                ).andReturn();
        String resultContent = result.getResponse().getContentAsString();
        FamilyTreeInfoResponse retrieved = om.readValue(resultContent, FamilyTreeInfoResponse.class);
        Assertions.assertEquals(retrieved, FamilyTreeInfoResponse.of(familyTree));
    }


    @Test
    void deleteFamilyTree_ok() throws Exception {
        familyTree.setId(1L);
        Mockito.when(service.deleteFamilyTreeById(familyTree.getId())).thenReturn(FamilyTreeInfoResponse.of(familyTree));
        MvcResult result = mockMvc.perform(delete("/api/family_tree/{id}", familyTree.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String resultContent = result.getResponse().getContentAsString();
        Long deletedId = om.readValue(resultContent, Long.class);
        Assertions.assertEquals(deletedId, familyTree.getId());
    }

    @Test
    void updateFamilyTree_ok() throws Exception {
        familyTree.setId(1L);
        FamilyTree updatedFamilyTree = new FamilyTree("ft1", "ft1 desc", user);

        FamilyTreeSaveUpdateRequest reqDto = new FamilyTreeSaveUpdateRequest(updatedFamilyTree.getTitle(), updatedFamilyTree.getDescription(), updatedFamilyTree.getUser().getId());

        Mockito.when(service.updateFamilyTree(familyTree.getId(), reqDto)).thenReturn(FamilyTreeInfoResponse.of(updatedFamilyTree));

        MvcResult result = mockMvc.perform(
                        put("/api/family_tree/{id}", familyTree.getId())
                                .content(om.writeValueAsString(reqDto))
                                .contentType(MediaType.APPLICATION_JSON_VALUE)
                                .characterEncoding(StandardCharsets.UTF_16)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpectAll(
                        status().isOk()
                )
                .andReturn();
        String resultContent = result.getResponse().getContentAsString();
        System.out.println("resultContent = " + resultContent);
        FamilyTreeInfoResponse updated = om.readValue(resultContent, FamilyTreeInfoResponse.class);

        Assertions.assertEquals(updated, FamilyTreeInfoResponse.of(updatedFamilyTree));
    }

    @Test
    void getAllFamilyTreesForUser() throws Exception {
        Mockito.when(service.getFamilyTreesByOwner(user.getId())).thenReturn(List.of(FamilyTreeInfoResponse.of(familyTree)));

        MvcResult result = mockMvc.perform(
                get("/api/family_tree/user/{user}", user.getId())
                        .characterEncoding(StandardCharsets.UTF_8)
                )
                .andDo(print()).andExpect(status().isOk()).andReturn();
        String resultContent = result.getResponse().getContentAsString();
        FamilyTreeInfoResponse[] retrieved = om.readValue(resultContent, FamilyTreeInfoResponse[].class);
        Assertions.assertEquals(Arrays.asList(retrieved), List.of(FamilyTreeInfoResponse.of(familyTree)));
    }
}